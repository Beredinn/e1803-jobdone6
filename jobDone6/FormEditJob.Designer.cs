﻿namespace jobDone6
{
    partial class FormEditJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label surfaceDimensionLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label dateStartLabel;
            System.Windows.Forms.Label dateStopLabel;
            System.Windows.Forms.Label jobTypeLabel;
            System.Windows.Forms.Label lensTypeLabel;
            System.Windows.Forms.Label commentLabel;
            System.Windows.Forms.Label orderCompanyLabel;
            System.Windows.Forms.Label orderNumberLabel;
            System.Windows.Forms.Label pieceMaterialLabel;
            System.Windows.Forms.Label jobDepthLabel;
            System.Windows.Forms.Label countPcsLabel;
            System.Windows.Forms.Label countLayerLabel;
            System.Windows.Forms.Label hatch1TypeLabel;
            System.Windows.Forms.Label hatch2TypeLabel;
            System.Windows.Forms.Label timeOfJobLabel;
            System.Windows.Forms.Label timeOfJobRealLabel;
            System.Windows.Forms.Label positionZLabel;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditJob));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.buttonSendValues = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            surfaceDimensionLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            dateStartLabel = new System.Windows.Forms.Label();
            dateStopLabel = new System.Windows.Forms.Label();
            jobTypeLabel = new System.Windows.Forms.Label();
            lensTypeLabel = new System.Windows.Forms.Label();
            commentLabel = new System.Windows.Forms.Label();
            orderCompanyLabel = new System.Windows.Forms.Label();
            orderNumberLabel = new System.Windows.Forms.Label();
            pieceMaterialLabel = new System.Windows.Forms.Label();
            jobDepthLabel = new System.Windows.Forms.Label();
            countPcsLabel = new System.Windows.Forms.Label();
            countLayerLabel = new System.Windows.Forms.Label();
            hatch1TypeLabel = new System.Windows.Forms.Label();
            hatch2TypeLabel = new System.Windows.Forms.Label();
            timeOfJobLabel = new System.Windows.Forms.Label();
            timeOfJobRealLabel = new System.Windows.Forms.Label();
            positionZLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(93, 410);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(48, 13);
            label1.TabIndex = 144;
            label1.Text = "Rozmiar:";
            // 
            // surfaceDimensionLabel
            // 
            surfaceDimensionLabel.AutoSize = true;
            surfaceDimensionLabel.Location = new System.Drawing.Point(68, 272);
            surfaceDimensionLabel.Name = "surfaceDimensionLabel";
            surfaceDimensionLabel.Size = new System.Drawing.Size(73, 13);
            surfaceDimensionLabel.TabIndex = 143;
            surfaceDimensionLabel.Text = "Powierzchnia:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(78, 19);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(63, 13);
            iDLabel.TabIndex = 126;
            iDLabel.Text = "Nr zlecenia:";
            // 
            // dateStartLabel
            // 
            dateStartLabel.AutoSize = true;
            dateStartLabel.Location = new System.Drawing.Point(69, 42);
            dateStartLabel.Name = "dateStartLabel";
            dateStartLabel.Size = new System.Drawing.Size(72, 13);
            dateStartLabel.TabIndex = 127;
            dateStartLabel.Text = "Rozpoczęcie:";
            // 
            // dateStopLabel
            // 
            dateStopLabel.AutoSize = true;
            dateStopLabel.Location = new System.Drawing.Point(69, 65);
            dateStopLabel.Name = "dateStopLabel";
            dateStopLabel.Size = new System.Drawing.Size(72, 13);
            dateStopLabel.TabIndex = 128;
            dateStopLabel.Text = "Zakończenie:";
            // 
            // jobTypeLabel
            // 
            jobTypeLabel.AutoSize = true;
            jobTypeLabel.Location = new System.Drawing.Point(71, 157);
            jobTypeLabel.Name = "jobTypeLabel";
            jobTypeLabel.Size = new System.Drawing.Size(70, 13);
            jobTypeLabel.TabIndex = 129;
            jobTypeLabel.Text = "Typ zlecenia:";
            // 
            // lensTypeLabel
            // 
            lensTypeLabel.AutoSize = true;
            lensTypeLabel.Location = new System.Drawing.Point(81, 226);
            lensTypeLabel.Name = "lensTypeLabel";
            lensTypeLabel.Size = new System.Drawing.Size(60, 13);
            lensTypeLabel.TabIndex = 130;
            lensTypeLabel.Text = "Soczewka:";
            // 
            // commentLabel
            // 
            commentLabel.AutoSize = true;
            commentLabel.Location = new System.Drawing.Point(81, 433);
            commentLabel.Name = "commentLabel";
            commentLabel.Size = new System.Drawing.Size(60, 13);
            commentLabel.TabIndex = 131;
            commentLabel.Text = "Komentarz:";
            // 
            // orderCompanyLabel
            // 
            orderCompanyLabel.AutoSize = true;
            orderCompanyLabel.Location = new System.Drawing.Point(58, 88);
            orderCompanyLabel.Name = "orderCompanyLabel";
            orderCompanyLabel.Size = new System.Drawing.Size(83, 13);
            orderCompanyLabel.TabIndex = 132;
            orderCompanyLabel.Text = "Zleceniodawca:";
            // 
            // orderNumberLabel
            // 
            orderNumberLabel.AutoSize = true;
            orderNumberLabel.Location = new System.Drawing.Point(47, 111);
            orderNumberLabel.Name = "orderNumberLabel";
            orderNumberLabel.Size = new System.Drawing.Size(94, 13);
            orderNumberLabel.TabIndex = 133;
            orderNumberLabel.Text = "Nr zleceniodawcy:";
            // 
            // pieceMaterialLabel
            // 
            pieceMaterialLabel.AutoSize = true;
            pieceMaterialLabel.Location = new System.Drawing.Point(92, 134);
            pieceMaterialLabel.Name = "pieceMaterialLabel";
            pieceMaterialLabel.Size = new System.Drawing.Size(49, 13);
            pieceMaterialLabel.TabIndex = 134;
            pieceMaterialLabel.Text = "Materiał:";
            // 
            // jobDepthLabel
            // 
            jobDepthLabel.AutoSize = true;
            jobDepthLabel.Location = new System.Drawing.Point(78, 180);
            jobDepthLabel.Name = "jobDepthLabel";
            jobDepthLabel.Size = new System.Drawing.Size(63, 13);
            jobDepthLabel.TabIndex = 135;
            jobDepthLabel.Text = "Głębokość:";
            // 
            // countPcsLabel
            // 
            countPcsLabel.AutoSize = true;
            countPcsLabel.Location = new System.Drawing.Point(81, 203);
            countPcsLabel.Name = "countPcsLabel";
            countPcsLabel.Size = new System.Drawing.Size(60, 13);
            countPcsLabel.TabIndex = 136;
            countPcsLabel.Text = "Ilość sztuk:";
            // 
            // countLayerLabel
            // 
            countLayerLabel.AutoSize = true;
            countLayerLabel.Location = new System.Drawing.Point(73, 249);
            countLayerLabel.Name = "countLayerLabel";
            countLayerLabel.Size = new System.Drawing.Size(68, 13);
            countLayerLabel.TabIndex = 137;
            countLayerLabel.Text = "Ilość warstw:";
            // 
            // hatch1TypeLabel
            // 
            hatch1TypeLabel.AutoSize = true;
            hatch1TypeLabel.Location = new System.Drawing.Point(75, 295);
            hatch1TypeLabel.Name = "hatch1TypeLabel";
            hatch1TypeLabel.Size = new System.Drawing.Size(66, 13);
            hatch1TypeLabel.TabIndex = 138;
            hatch1TypeLabel.Text = "Hatch1 Typ:";
            // 
            // hatch2TypeLabel
            // 
            hatch2TypeLabel.AutoSize = true;
            hatch2TypeLabel.Location = new System.Drawing.Point(75, 318);
            hatch2TypeLabel.Name = "hatch2TypeLabel";
            hatch2TypeLabel.Size = new System.Drawing.Size(66, 13);
            hatch2TypeLabel.TabIndex = 139;
            hatch2TypeLabel.Text = "Hatch2 Typ:";
            // 
            // timeOfJobLabel
            // 
            timeOfJobLabel.AutoSize = true;
            timeOfJobLabel.Location = new System.Drawing.Point(52, 341);
            timeOfJobLabel.Name = "timeOfJobLabel";
            timeOfJobLabel.Size = new System.Drawing.Size(89, 13);
            timeOfJobLabel.TabIndex = 140;
            timeOfJobLabel.Text = "Czas szacowany:";
            // 
            // timeOfJobRealLabel
            // 
            timeOfJobRealLabel.AutoSize = true;
            timeOfJobRealLabel.Location = new System.Drawing.Point(52, 364);
            timeOfJobRealLabel.Name = "timeOfJobRealLabel";
            timeOfJobRealLabel.Size = new System.Drawing.Size(89, 13);
            timeOfJobRealLabel.TabIndex = 141;
            timeOfJobRealLabel.Text = "Czas rzeczywisty:";
            // 
            // positionZLabel
            // 
            positionZLabel.AutoSize = true;
            positionZLabel.Location = new System.Drawing.Point(71, 387);
            positionZLabel.Name = "positionZLabel";
            positionZLabel.Size = new System.Drawing.Size(70, 13);
            positionZLabel.TabIndex = 142;
            positionZLabel.Text = "Wysokość Z:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(113, 500);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(28, 13);
            label2.TabIndex = 165;
            label2.Text = "Fnc:";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(144, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 20);
            this.textBox1.TabIndex = 145;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(144, 39);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(150, 20);
            this.textBox2.TabIndex = 146;
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(144, 62);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(150, 20);
            this.textBox3.TabIndex = 147;
            this.textBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(144, 85);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(150, 20);
            this.textBox4.TabIndex = 148;
            this.textBox4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(144, 108);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(150, 20);
            this.textBox5.TabIndex = 149;
            this.textBox5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(144, 131);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(150, 20);
            this.textBox6.TabIndex = 150;
            this.textBox6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(144, 154);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(150, 20);
            this.textBox7.TabIndex = 151;
            this.textBox7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(144, 177);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(150, 20);
            this.textBox8.TabIndex = 152;
            this.textBox8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(144, 200);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(150, 20);
            this.textBox9.TabIndex = 153;
            this.textBox9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(144, 223);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(150, 20);
            this.textBox10.TabIndex = 154;
            this.textBox10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(144, 246);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(150, 20);
            this.textBox11.TabIndex = 155;
            this.textBox11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(144, 269);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(150, 20);
            this.textBox12.TabIndex = 156;
            this.textBox12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(144, 292);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(150, 20);
            this.textBox13.TabIndex = 157;
            this.textBox13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(144, 315);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(150, 20);
            this.textBox14.TabIndex = 158;
            this.textBox14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(144, 338);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(150, 20);
            this.textBox15.TabIndex = 159;
            this.textBox15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(144, 361);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(150, 20);
            this.textBox16.TabIndex = 160;
            this.textBox16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(144, 384);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(150, 20);
            this.textBox17.TabIndex = 161;
            this.textBox17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(144, 407);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(150, 20);
            this.textBox18.TabIndex = 162;
            this.textBox18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(144, 430);
            this.textBox19.Multiline = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(150, 64);
            this.textBox19.TabIndex = 163;
            this.textBox19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(144, 497);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(150, 20);
            this.textBox20.TabIndex = 164;
            this.textBox20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // buttonSendValues
            // 
            this.buttonSendValues.Location = new System.Drawing.Point(167, 524);
            this.buttonSendValues.Name = "buttonSendValues";
            this.buttonSendValues.Size = new System.Drawing.Size(75, 23);
            this.buttonSendValues.TabIndex = 166;
            this.buttonSendValues.Text = "Zatwierdź zmiany";
            this.buttonSendValues.UseVisualStyleBackColor = true;
            this.buttonSendValues.Click += new System.EventHandler(this.buttonSendValues_Click);
            // 
            // FormEditJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 562);
            this.Controls.Add(this.buttonSendValues);
            this.Controls.Add(label2);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(label1);
            this.Controls.Add(surfaceDimensionLabel);
            this.Controls.Add(iDLabel);
            this.Controls.Add(dateStartLabel);
            this.Controls.Add(dateStopLabel);
            this.Controls.Add(jobTypeLabel);
            this.Controls.Add(lensTypeLabel);
            this.Controls.Add(commentLabel);
            this.Controls.Add(orderCompanyLabel);
            this.Controls.Add(orderNumberLabel);
            this.Controls.Add(pieceMaterialLabel);
            this.Controls.Add(jobDepthLabel);
            this.Controls.Add(countPcsLabel);
            this.Controls.Add(countLayerLabel);
            this.Controls.Add(hatch1TypeLabel);
            this.Controls.Add(hatch2TypeLabel);
            this.Controls.Add(timeOfJobLabel);
            this.Controls.Add(timeOfJobRealLabel);
            this.Controls.Add(positionZLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(328, 600);
            this.MinimumSize = new System.Drawing.Size(328, 600);
            this.Name = "FormEditJob";
            this.Text = "jobDone - Edycja wpisu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Button buttonSendValues;
    }
}