﻿namespace jobDone6
{
    partial class FormPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrint));
            this.radioButtonChooseID = new System.Windows.Forms.RadioButton();
            this.radioButtonChooseMonth = new System.Windows.Forms.RadioButton();
            this.radioButtonChooseDate = new System.Windows.Forms.RadioButton();
            this.textBoxIDfrom = new System.Windows.Forms.TextBox();
            this.textBoxIDto = new System.Windows.Forms.TextBox();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.comboBoxMonth = new System.Windows.Forms.ComboBox();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelCountPage = new System.Windows.Forms.Label();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonSetupPage = new System.Windows.Forms.Button();
            this.buttonShowPrintDialog = new System.Windows.Forms.Button();
            this.buttonPrintPreview = new System.Windows.Forms.Button();
            this.buttonGenrePrintableFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radioButtonChooseID
            // 
            this.radioButtonChooseID.AutoSize = true;
            this.radioButtonChooseID.Checked = true;
            this.radioButtonChooseID.Location = new System.Drawing.Point(13, 13);
            this.radioButtonChooseID.Name = "radioButtonChooseID";
            this.radioButtonChooseID.Size = new System.Drawing.Size(156, 17);
            this.radioButtonChooseID.TabIndex = 0;
            this.radioButtonChooseID.TabStop = true;
            this.radioButtonChooseID.Text = "Drukuj po numerze zlecenia";
            this.radioButtonChooseID.UseVisualStyleBackColor = true;
            this.radioButtonChooseID.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonChooseMonth
            // 
            this.radioButtonChooseMonth.AutoSize = true;
            this.radioButtonChooseMonth.Location = new System.Drawing.Point(13, 84);
            this.radioButtonChooseMonth.Name = "radioButtonChooseMonth";
            this.radioButtonChooseMonth.Size = new System.Drawing.Size(118, 17);
            this.radioButtonChooseMonth.TabIndex = 1;
            this.radioButtonChooseMonth.Text = "Drukuj cały miesiąc";
            this.radioButtonChooseMonth.UseVisualStyleBackColor = true;
            this.radioButtonChooseMonth.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonChooseDate
            // 
            this.radioButtonChooseDate.AutoSize = true;
            this.radioButtonChooseDate.Location = new System.Drawing.Point(13, 36);
            this.radioButtonChooseDate.Name = "radioButtonChooseDate";
            this.radioButtonChooseDate.Size = new System.Drawing.Size(100, 17);
            this.radioButtonChooseDate.TabIndex = 2;
            this.radioButtonChooseDate.Text = "Drukuj po dacie";
            this.radioButtonChooseDate.UseVisualStyleBackColor = true;
            this.radioButtonChooseDate.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // textBoxIDfrom
            // 
            this.textBoxIDfrom.Location = new System.Drawing.Point(208, 12);
            this.textBoxIDfrom.Name = "textBoxIDfrom";
            this.textBoxIDfrom.Size = new System.Drawing.Size(67, 20);
            this.textBoxIDfrom.TabIndex = 3;
            // 
            // textBoxIDto
            // 
            this.textBoxIDto.Location = new System.Drawing.Point(295, 12);
            this.textBoxIDto.Name = "textBoxIDto";
            this.textBoxIDto.Size = new System.Drawing.Size(64, 20);
            this.textBoxIDto.TabIndex = 4;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Enabled = false;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(208, 36);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerFrom.TabIndex = 5;
            // 
            // comboBoxMonth
            // 
            this.comboBoxMonth.Enabled = false;
            this.comboBoxMonth.FormattingEnabled = true;
            this.comboBoxMonth.Location = new System.Drawing.Point(208, 84);
            this.comboBoxMonth.Name = "comboBoxMonth";
            this.comboBoxMonth.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMonth.TabIndex = 6;
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Enabled = false;
            this.dateTimePickerTo.Location = new System.Drawing.Point(208, 60);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerTo.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "od";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "do";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "włącznie";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(361, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "włącznie";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(188, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "od";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(188, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "do";
            // 
            // labelCountPage
            // 
            this.labelCountPage.AutoSize = true;
            this.labelCountPage.Location = new System.Drawing.Point(208, 137);
            this.labelCountPage.Name = "labelCountPage";
            this.labelCountPage.Size = new System.Drawing.Size(87, 13);
            this.labelCountPage.TabIndex = 14;
            this.labelCountPage.Text = "Wybierz wartości";
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(227, 219);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(120, 23);
            this.buttonPrint.TabIndex = 15;
            this.buttonPrint.Text = "Drukuj szybko";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonSetupPage
            // 
            this.buttonSetupPage.Location = new System.Drawing.Point(75, 219);
            this.buttonSetupPage.Name = "buttonSetupPage";
            this.buttonSetupPage.Size = new System.Drawing.Size(120, 23);
            this.buttonSetupPage.TabIndex = 16;
            this.buttonSetupPage.Text = "Ustawienia strony";
            this.buttonSetupPage.UseVisualStyleBackColor = true;
            this.buttonSetupPage.Click += new System.EventHandler(this.buttonSetupPage_Click);
            // 
            // buttonShowPrintDialog
            // 
            this.buttonShowPrintDialog.Location = new System.Drawing.Point(227, 190);
            this.buttonShowPrintDialog.Name = "buttonShowPrintDialog";
            this.buttonShowPrintDialog.Size = new System.Drawing.Size(120, 23);
            this.buttonShowPrintDialog.TabIndex = 18;
            this.buttonShowPrintDialog.Text = "Drukowanie";
            this.buttonShowPrintDialog.UseVisualStyleBackColor = true;
            this.buttonShowPrintDialog.Click += new System.EventHandler(this.buttonShowPrintDialog_Click);
            // 
            // buttonPrintPreview
            // 
            this.buttonPrintPreview.Location = new System.Drawing.Point(75, 190);
            this.buttonPrintPreview.Name = "buttonPrintPreview";
            this.buttonPrintPreview.Size = new System.Drawing.Size(120, 23);
            this.buttonPrintPreview.TabIndex = 17;
            this.buttonPrintPreview.Text = "Podgląd strony";
            this.buttonPrintPreview.UseVisualStyleBackColor = true;
            this.buttonPrintPreview.Click += new System.EventHandler(this.buttonPrintPreview_Click);
            // 
            // buttonGenrePrintableFile
            // 
            this.buttonGenrePrintableFile.Location = new System.Drawing.Point(208, 111);
            this.buttonGenrePrintableFile.Name = "buttonGenrePrintableFile";
            this.buttonGenrePrintableFile.Size = new System.Drawing.Size(120, 23);
            this.buttonGenrePrintableFile.TabIndex = 19;
            this.buttonGenrePrintableFile.Text = "Odśwież";
            this.buttonGenrePrintableFile.UseVisualStyleBackColor = true;
            this.buttonGenrePrintableFile.Click += new System.EventHandler(this.buttonGenrePrintableFile_Click);
            // 
            // FormPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 259);
            this.Controls.Add(this.buttonGenrePrintableFile);
            this.Controls.Add(this.buttonShowPrintDialog);
            this.Controls.Add(this.buttonPrintPreview);
            this.Controls.Add(this.buttonSetupPage);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.labelCountPage);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePickerTo);
            this.Controls.Add(this.comboBoxMonth);
            this.Controls.Add(this.dateTimePickerFrom);
            this.Controls.Add(this.textBoxIDto);
            this.Controls.Add(this.textBoxIDfrom);
            this.Controls.Add(this.radioButtonChooseDate);
            this.Controls.Add(this.radioButtonChooseMonth);
            this.Controls.Add(this.radioButtonChooseID);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPrint";
            this.Text = "Drukuj serie zleceń";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonChooseID;
        private System.Windows.Forms.RadioButton radioButtonChooseMonth;
        private System.Windows.Forms.RadioButton radioButtonChooseDate;
        private System.Windows.Forms.TextBox textBoxIDfrom;
        private System.Windows.Forms.TextBox textBoxIDto;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.ComboBox comboBoxMonth;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelCountPage;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonSetupPage;
        private System.Windows.Forms.Button buttonShowPrintDialog;
        private System.Windows.Forms.Button buttonPrintPreview;
        private System.Windows.Forms.Button buttonGenrePrintableFile;
    }
}