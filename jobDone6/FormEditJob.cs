﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jobDone6
{
    public partial class FormEditJob : Form
    {
        public string returnAfterText;
        public string queryListOfChange;

        private string edit_1;
        private string edit_2;
        private string edit_3;
        private string edit_4;
        private string edit_5;
        private string edit_6;
        private string edit_7;
        private string edit_8;
        private string edit_9;
        private string edit_10;
        private string edit_11;
        private string edit_12;
        private string edit_13;
        private string edit_14;
        private string edit_15;
        private string edit_16;
        private string edit_17;
        private string edit_18;
        private string edit_19;
        private string edit_20;


        public FormEditJob( string _edit_1,
                            string _edit_2,
                            string _edit_3,
                            string _edit_4,
                            string _edit_5,
                            string _edit_6,
                            string _edit_7,
                            string _edit_8,
                            string _edit_9,
                            string _edit_10,
                            string _edit_11,
                            string _edit_12,
                            string _edit_13,
                            string _edit_14,
                            string _edit_15,
                            string _edit_16,
                            string _edit_17,
                            string _edit_18,
                            string _edit_19,
                            string _edit_20
            )
        {
            InitializeComponent();
            edit_1 = textBox1.Text = _edit_1;
            edit_2 = textBox2.Text = _edit_2;
            edit_3 = textBox3.Text = _edit_3;
            edit_4 = textBox4.Text = _edit_4;
            edit_5 = textBox5.Text = _edit_5;
            edit_6 = textBox6.Text = _edit_6;
            edit_7 = textBox7.Text = _edit_7;
            edit_8 = textBox8.Text = _edit_8;
            edit_9 = textBox9.Text = _edit_9;
            edit_10 = textBox10.Text = _edit_10;
            edit_11 = textBox11.Text = _edit_11;
            edit_12 = textBox12.Text = _edit_12;
            edit_13 = textBox13.Text = _edit_13;
            edit_14 = textBox14.Text = _edit_14;
            edit_15 = textBox15.Text = _edit_15;
            edit_16 = textBox16.Text = _edit_16;
            edit_17 = textBox17.Text = _edit_17;
            edit_18 = textBox19.Text = _edit_18;
            edit_19 = textBox18.Text = _edit_19;
            edit_20 = textBox20.Text = _edit_20;
        }

        private void buttonSendValues_Click(object sender, EventArgs e)
        {
            sendValues();
        }

        private void sendValues()
        {
            queryListOfChange = textBox1.Text;
            queryListOfChange += "; New data:";
            if (textBox2.Text != edit_2) queryListOfChange += " datestart(" + edit_2 + "->" + textBox2.Text + ")";
            if (textBox3.Text != edit_3) queryListOfChange += " datestop(" + edit_3 + "->" + textBox3.Text + ")";
            if (textBox4.Text != edit_4) queryListOfChange += " ordercompany(" + edit_4 + "->" + textBox4.Text + ")";
            if (textBox5.Text != edit_5) queryListOfChange += " orderID(" + edit_5 + "->" + textBox5.Text + ")";
            if (textBox6.Text != edit_6) queryListOfChange += " material(" + edit_6 + "->" + textBox6.Text + ")";
            if (textBox7.Text != edit_7) queryListOfChange += " jobType(" + edit_7 + "->" + textBox7.Text + ")";
            if (textBox8.Text != edit_8) queryListOfChange += " jobDepth(" + edit_8 + "->" + textBox8.Text + ")";
            if (textBox9.Text != edit_9) queryListOfChange += " countPcs(" + edit_9 + "->" + textBox9.Text + ")";
            if (textBox10.Text != edit_10) queryListOfChange += " lensType(" + edit_10 + "->" + textBox10.Text + ")";
            if (textBox11.Text != edit_11) queryListOfChange += " countLayer(" + edit_11 + "->" + textBox11.Text + ")";
            if (textBox12.Text != edit_12) queryListOfChange += " surfaceDimension(" + edit_12 + "->" + textBox12.Text + ")";
            if (textBox13.Text != edit_13) queryListOfChange += " hatch1type(" + edit_13 + "->" + textBox13.Text + ")";
            if (textBox14.Text != edit_14) queryListOfChange += " hatch2type(" + edit_14 + "->" + textBox14.Text + ")";
            if (textBox15.Text != edit_15) queryListOfChange += " timeValuate(" + edit_15 + "->" + textBox15.Text + ")";
            if (textBox16.Text != edit_16) queryListOfChange += " timeReal(" + edit_16 + "->" + textBox16.Text + ")";
            if (textBox17.Text != edit_17) queryListOfChange += " positionZAxis(" + edit_17 + "->" + textBox17.Text + ")";
            if (textBox18.Text != edit_18) queryListOfChange += " comment(" + edit_18 + "->" + textBox18.Text + ")";
            if (textBox19.Text != edit_19) queryListOfChange += " dimension(" + edit_19 + "->" + textBox19.Text + ")";
            if (textBox20.Text != edit_20) queryListOfChange += " help1(" + edit_20 + "->" + textBox20.Text + ")";



            returnAfterText = "UPDATE `job` SET `ID` = " + textBox1.Text +
            ", `dateStart` = '" + textBox2.Text +
            "', `dateStop` = '" + textBox3.Text +
            "', `orderCompany` = '" + textBox4.Text +
            "', `orderID` = '" + textBox5.Text +
            "', `material` = '" + textBox6.Text +
            "', `jobType` = '" + textBox7.Text +
            "', `jobDepth` = '" + textBox8.Text +
            "', `countPcs` = '" + textBox9.Text +
            "', `lensType` = '" + textBox10.Text +
            "', `countLayer` = '" + textBox11.Text +
            "', `surfaceDimension` = '" + textBox12.Text +
            "', `hatch1type` = '" + textBox13.Text +
            "', `hatch2type` = '" + textBox14.Text +
            "', `timeValuate` = '" + textBox15.Text +
            "', `timeReal` = '" + textBox16.Text +
            "', `positionZAxis` = '" + textBox17.Text +
            "', `comment` = '" + textBox19.Text +
            "', `dimension` = '" + textBox18.Text +
            "', `help1` = '" + textBox20.Text +
            "' WHERE `job`.`ID` = " + textBox1.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void checkEnter(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                sendValues();
            }
        }
    }
}
