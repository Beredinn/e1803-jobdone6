﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jobDone6
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void buttonLoginTest_Click(object sender, EventArgs e)
        {
            
        }

        private void checkEnter(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                closeThisWindow();
            }
        }

        private void closeThisWindow()
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
