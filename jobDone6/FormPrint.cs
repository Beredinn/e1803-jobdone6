﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jobDone6
{
    public partial class FormPrint : Form
    {

        public FormPrint()
        {
            InitializeComponent();

            //myWebBrowser.DocumentText = System.IO.File.ReadAllText(@"C:\engraver\database\html\printready.html");

        }


        WebBrowser myWebBrowser = new WebBrowser();


        private void buttonPrint_Click(object sender, EventArgs e)
        {
            myWebBrowser.Print();
        }

        private void buttonShowPrintDialog_Click(object sender, EventArgs e)
        {
            myWebBrowser.ShowPrintDialog();
        }

        private void buttonSetupPage_Click(object sender, EventArgs e)
        {
            myWebBrowser.ShowPageSetupDialog();
        }

        private void buttonPrintPreview_Click(object sender, EventArgs e)
        {
            myWebBrowser.ShowPrintPreviewDialog();

        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonChooseID.Checked)
            {
                textBoxIDfrom.Enabled = true;
                textBoxIDto.Enabled = true;
            }
            else
            {
                textBoxIDfrom.Enabled = false;
                textBoxIDto.Enabled = false;
            }

            if (radioButtonChooseDate.Checked)
            {
                dateTimePickerFrom.Enabled = true;
                dateTimePickerTo.Enabled = true;
            }
            else
            {
                dateTimePickerFrom.Enabled = false;
                dateTimePickerTo.Enabled = false;
            }

            if (radioButtonChooseMonth.Checked)
            {
                comboBoxMonth.Enabled = true;
            }
            else
            {
                comboBoxMonth.Enabled = false;
            }

        }

        private void buttonGenrePrintableFile_Click(object sender, EventArgs e)
        {
            
            openHtml();
        }

        private void openHtml()
        {

            myWebBrowser.Navigate(@"C:\engraver\database\html\printready.html");
           // myWebBrowser.Show();
        }
    }
}
