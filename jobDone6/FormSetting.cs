﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jobDone6
{
    public partial class FormSetting : Form
    {
        public FormSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(@"C:\engraver\database\temp\1_job.bmp");
            var black = 0;
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    Color color = bmp.GetPixel(x, y);

                    if (bmp.GetPixel(x, y).ToArgb().Equals(Color.Black.ToArgb()))
                    {
                        black++;
                    }
                }
            }
            label1.Text = Convert.ToString(black/250);
            bmp.Dispose();
        }
    }
}
