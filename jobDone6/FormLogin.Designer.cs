﻿namespace jobDone6
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.buttonLoginTest = new System.Windows.Forms.Button();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.maskedTextBoxPass = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDBIP = new System.Windows.Forms.TextBox();
            this.textBoxDBPort = new System.Windows.Forms.TextBox();
            this.textBoxDBDB = new System.Windows.Forms.TextBox();
            this.textBoxDBSSL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonLoginTest
            // 
            this.buttonLoginTest.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonLoginTest.Location = new System.Drawing.Point(50, 170);
            this.buttonLoginTest.Name = "buttonLoginTest";
            this.buttonLoginTest.Size = new System.Drawing.Size(75, 23);
            this.buttonLoginTest.TabIndex = 20;
            this.buttonLoginTest.Text = "Zaloguj";
            this.buttonLoginTest.UseVisualStyleBackColor = true;
            this.buttonLoginTest.Click += new System.EventHandler(this.buttonLoginTest_Click);
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(50, 10);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(100, 20);
            this.textBoxLogin.TabIndex = 1;
            this.textBoxLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // maskedTextBoxPass
            // 
            this.maskedTextBoxPass.Location = new System.Drawing.Point(50, 37);
            this.maskedTextBoxPass.Name = "maskedTextBoxPass";
            this.maskedTextBoxPass.PasswordChar = '*';
            this.maskedTextBoxPass.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBoxPass.TabIndex = 2;
            this.maskedTextBoxPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Login:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Hasło:";
            // 
            // textBoxDBIP
            // 
            this.textBoxDBIP.Location = new System.Drawing.Point(50, 64);
            this.textBoxDBIP.Name = "textBoxDBIP";
            this.textBoxDBIP.Size = new System.Drawing.Size(100, 20);
            this.textBoxDBIP.TabIndex = 5;
            this.textBoxDBIP.Text = "127.0.0.1";
            this.textBoxDBIP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBoxDBPort
            // 
            this.textBoxDBPort.Location = new System.Drawing.Point(50, 91);
            this.textBoxDBPort.Name = "textBoxDBPort";
            this.textBoxDBPort.Size = new System.Drawing.Size(100, 20);
            this.textBoxDBPort.TabIndex = 6;
            this.textBoxDBPort.Text = "3306";
            this.textBoxDBPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBoxDBDB
            // 
            this.textBoxDBDB.Location = new System.Drawing.Point(50, 118);
            this.textBoxDBDB.Name = "textBoxDBDB";
            this.textBoxDBDB.Size = new System.Drawing.Size(100, 20);
            this.textBoxDBDB.TabIndex = 7;
            this.textBoxDBDB.Text = "jobdonedb";
            this.textBoxDBDB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // textBoxDBSSL
            // 
            this.textBoxDBSSL.Location = new System.Drawing.Point(50, 144);
            this.textBoxDBSSL.Name = "textBoxDBSSL";
            this.textBoxDBSSL.Size = new System.Drawing.Size(100, 20);
            this.textBoxDBSSL.TabIndex = 8;
            this.textBoxDBSSL.Text = "none";
            this.textBoxDBSSL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "IP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Port:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "DB:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "SSL:";
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(163, 208);
            this.ControlBox = false;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxDBSSL);
            this.Controls.Add(this.textBoxDBDB);
            this.Controls.Add(this.textBoxDBPort);
            this.Controls.Add(this.textBoxDBIP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.maskedTextBoxPass);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(this.buttonLoginTest);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(179, 246);
            this.MinimumSize = new System.Drawing.Size(179, 246);
            this.Name = "FormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zaloguj do programu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoginTest;
        public System.Windows.Forms.TextBox textBoxLogin;
        public System.Windows.Forms.MaskedTextBox maskedTextBoxPass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBoxDBIP;
        public System.Windows.Forms.TextBox textBoxDBPort;
        public System.Windows.Forms.TextBox textBoxDBDB;
        public System.Windows.Forms.TextBox textBoxDBSSL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}