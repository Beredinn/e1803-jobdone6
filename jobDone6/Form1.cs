﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using SAMLIGHT_CLIENT_CTRL_OCXLib;

using System.IO;

using MySql.Data.MySqlClient;

namespace jobDone6
{
    public partial class Form1 : Form
    {

        string mySQLConnectionString = "datasource=127.0.0.1;port=3306;username=emil;password=emil123;database=jobdonedb;SslMode=none";


        public Form1()
        {
            InitializeComponent();

            loginMenu();
        }

        private void loginMenu()
        {

            FormLogin FormLogin_1 = new FormLogin();
            FormLogin_1.ShowDialog();

            if (FormLogin_1.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                mySQLUserName = FormLogin_1.textBoxLogin.Text;
                mySQLUserPass = FormLogin_1.maskedTextBoxPass.Text;

                mySQLConnectionString = "datasource=" + FormLogin_1.textBoxDBIP.Text;
                mySQLConnectionString += ";port=" + FormLogin_1.textBoxDBPort.Text;
                if (FormLogin_1.maskedTextBoxPass.Text != "")
                {
                    mySQLConnectionString += ";username=" + FormLogin_1.textBoxLogin.Text;
                    mySQLConnectionString += ";password=" + FormLogin_1.maskedTextBoxPass.Text;
                }
                else
                {
                    mySQLConnectionString += ";username=show";
                    mySQLConnectionString += ";password=show";
                }
                mySQLConnectionString += ";database=" + FormLogin_1.textBoxDBDB.Text;
                mySQLConnectionString += ";SslMode=" + FormLogin_1.textBoxDBSSL.Text;
                jobDBRefreshFunction();

                allTextBoxDisable();


                buttonAddFileToThisJob.Enabled = false;
                buttonFinishNewJob.Enabled = false;
                buttonNextStep.Enabled = false;
                buttonAddNewJob.Enabled = true;

                timer1.Enabled = true;
            }
        }

        private void jobDBRefresh(object sender, EventArgs e)
        {
            jobDBRefreshFunction();
        }

        private void jobDBRefreshFunction()
        {
            runDBQueryReturnToDataGrid("SELECT * FROM `job`");

            runDBQueryReturnToCombobox(1);
            runDBQueryReturnToCombobox(2);
            runDBQueryReturnToCombobox(3);
            runDBQueryReturnToCombobox(4);
            runDBQueryReturnToCombobox(5);
        }


        private void jobDBRefresh()
        {
            runDBQueryReturnToDataGrid("SELECT * FROM `job`");
        }

        private void runDBQueryReturnToCombobox(int comboBoxNumer)
        {
            labelPrintReady.Text = "";

            string query = "";
            if (comboBoxNumer == 1)
            {
                query = "SELECT * FROM `listOfCompany`";
                orderCompanyComboBox.Items.Clear();
                findCompanyComboBox.Items.Clear();
            }
            if (comboBoxNumer == 2)
            {
                query = "SELECT * FROM `listOfMaterial`";
                pieceMaterialComboBox.Items.Clear();
                findMaterialComboBox.Items.Clear();
            }
            if (comboBoxNumer == 3)
            {
                query = "SELECT * FROM `listOfJobType`";
                jobTypeComboBox.Items.Clear();
                findJobTypeComboBox.Items.Clear();
            }
            if (comboBoxNumer == 4)
            {
                query = "SELECT * FROM `listOfJobDepth`";
                jobDepthComboBox.Items.Clear();
                findJobDepthComboBox.Items.Clear();
            }
            if (comboBoxNumer == 5)
            {
                query = "SELECT * FROM `listOfLensType`";
                lensTypeComboBox.Items.Clear();
                findLensTypeComboBox.Items.Clear();
            }

            MySqlConnection connectionDB = new MySqlConnection(mySQLConnectionString);

            MySqlCommand commandDB = new MySqlCommand(query, connectionDB);

            commandDB.CommandTimeout = 60;

            try
            {
                connectionDB.Open();

                MySqlDataReader myReader = commandDB.ExecuteReader();
                if (myReader.HasRows)
                {
                    while (myReader.Read())
                    {
                        if (comboBoxNumer == 1)
                        {
                            orderCompanyComboBox.Items.Add(myReader.GetString(1));
                            findCompanyComboBox.Items.Add(myReader.GetString(1));
                        }
                        if (comboBoxNumer == 2)
                        { 
                            pieceMaterialComboBox.Items.Add(myReader.GetString(1));
                            findMaterialComboBox.Items.Add(myReader.GetString(1));
                        }
                        if (comboBoxNumer == 3)
                        { 
                            jobTypeComboBox.Items.Add(myReader.GetString(1));
                            findJobTypeComboBox.Items.Add(myReader.GetString(1));
                        }
                        if (comboBoxNumer == 4)
                        {
                            jobDepthComboBox.Items.Add(myReader.GetString(1));
                            findJobDepthComboBox.Items.Add(myReader.GetString(1));
                        }
                        if (comboBoxNumer == 5)
                        { 
                            lensTypeComboBox.Items.Add(myReader.GetString(1));
                            findLensTypeComboBox.Items.Add(myReader.GetString(1));
                        }
                                                    
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("błąd zapytania " + ex.Message);
            }

        }
/////////////////////////////////

        private string runDBQueryReturnVal(string query)
        {
            labelPrintReady.Text = "";


            if (query == "")
            {
                MessageBox.Show("brak zapytania MySQL");
                return "0";
            }

            MySqlConnection connectionDB = new MySqlConnection(mySQLConnectionString);

            MySqlCommand commandDB = new MySqlCommand(query, connectionDB);

            commandDB.CommandTimeout = 60;

            try
            {
                connectionDB.Open();

                MySqlDataReader myReader = commandDB.ExecuteReader();

                if (myReader.HasRows)
                {
                    while (myReader.Read())
                    {
                        return myReader.GetString(0);

                    }
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("błąd zapytania " + ex.Message);
                return "0";
            }
            return "0";

            
        }

        private void runDBQueryReturnToDataGrid(string query)
        {

            labelPrintReady.Text = "";



            if (query == "")
            {
                MessageBox.Show("brak zapytania MySQL");
                return;
            }

            MySqlConnection connectionDB = new MySqlConnection(mySQLConnectionString);

            MySqlCommand commandDB = new MySqlCommand(query, connectionDB);

            commandDB.CommandTimeout = 60;

            try
            {
                dataGridDB.Rows.Clear();
                connectionDB.Open();

                MySqlDataReader myReader = commandDB.ExecuteReader();

                if (myReader.HasRows)
                {
                    while (myReader.Read())
                    {
                        if (myReader.GetString(19) == "ok")
                        {
                            dataGridDB.Rows.Add();
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["ID"].Value = myReader.GetString(0);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["dateStart"].Value = myReader.GetString(1);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["dateStop"].Value = myReader.GetString(2);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["orderCompany"].Value = myReader.GetString(3);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["orderID"].Value = myReader.GetString(4);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["material"].Value = myReader.GetString(5);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["jobType"].Value = myReader.GetString(6);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["jobDepth"].Value = myReader.GetString(7);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["countPcs"].Value = myReader.GetString(8);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["lensType"].Value = myReader.GetString(9);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["countLayer"].Value = myReader.GetString(10);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["surfaceDimension"].Value = myReader.GetString(11);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["hatch1type"].Value = myReader.GetString(12);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["hatch2type"].Value = myReader.GetString(13);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["timeValuate"].Value = myReader.GetString(14);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["timeReal"].Value = myReader.GetString(15);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["positionZAxis"].Value = myReader.GetString(16);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["comment"].Value = myReader.GetString(17);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["dimension"].Value = myReader.GetString(18);
                            dataGridDB.Rows[dataGridDB.RowCount - 1].Cells["help1"].Value = myReader.GetString(19);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("błąd zapytania " + ex.Message);
                DialogResult dialog = MessageBox.Show("Błąd zapytania MySQL. Upewnij isę, że wpisałeś dobre wartości", "Zarządzanie bazą danych", MessageBoxButtons.OK);

            }

        }

        private string user = "anonymus";

        private void buttonNextStep_Click(object sender, EventArgs e)
        {



            if (orderNumberTextBox.Text == "")
            {
                orderNumberTextBox.Text = "-";
            }

            if (commentTextBox.Text == "")
            {
                commentTextBox.Text = "-";
            }
            
            if (!(
                (orderCompanyComboBox.Text == "") ||
                (pieceMaterialComboBox.Text == "") ||
                (jobTypeComboBox.Text == "") ||
                (jobDepthComboBox.Text == "") ||
                (countPcsTextBox.Text == "") ||
                (lensTypeComboBox.Text == "") ||
                (countLayerTextBox.Text == "") ||
                (surfaceDimensionTextBox.Text == "") ||
                (hatch1TypeTextBox.Text == "") ||
                (hatch2TypeTextBox.Text == "") ||
                (timeOfJobTextBox.Text == "") ||
                (timeOfJobRealTextBox.Text == "") ||
                (positionZTextBox.Text == "") ||
                (DimensionTextBox.Text == "")
            ))
            {
                int numerid = 1+ Convert.ToInt16(runDBQueryReturnVal("SELECT MAX(ID) FROM `job`"));
                iDLabel1.Text = numerid.ToString();
                string query = "";
                query += "INSERT INTO `job`(`ID`, `dateStart`, `dateStop`, `orderCompany`, `orderID`, `material`, `jobType`, `jobDepth`, `countPcs`, `lensType`, `countLayer`, `surfaceDimension`, `hatch1type`, `hatch2type`, `timeValuate`, `timeReal`, `positionZAxis`, `comment`, `dimension`, `help1`)";
                query += "VALUES (";
                query += iDLabel1.Text + ", \"";
                query += dateStartDateTimePicker.Value + "\", \"";
                query += dateStopDateTimePicker.Value + "\", \"";
                query += orderCompanyComboBox.Text + "\", \"";
                query += orderNumberTextBox.Text + "\", \"";
                query += pieceMaterialComboBox.Text + "\", \"";
                query += jobTypeComboBox.Text + "\", \"";
                query += jobDepthComboBox.Text + "\", \"";
                query += countPcsTextBox.Text + "\", \"";
                query += lensTypeComboBox.Text + "\", \"";
                query += countLayerTextBox.Text + "\", \"";
                query += surfaceDimensionTextBox.Text + "\", \"";
                query += hatch1TypeTextBox.Text + "\", \"";
                query += hatch2TypeTextBox.Text + "\", \"";
                query += timeOfJobTextBox.Text + "\", \"";
                query += timeOfJobRealTextBox.Text + "\", \"";
                query += positionZTextBox.Text + "\", \"";
                query += commentTextBox.Text + "\", \"";
                query += DimensionTextBox.Text + "\", \"";
                query += "ok" + "\")";

                runDBQueryReturnToDataGrid(query);

                runDBQueryReturnVal("INSERT INTO `historyeditjob` (`ID`, `user`, `date`, `history`) VALUES (NULL, '"+
                                        user +"', '"+
                                        DateTime.Now +"', 'Add new job; ID="+
                                        iDLabel1.Text+"')");

                buttonAddFileToThisJob.Enabled = false;
                buttonFinishNewJob.Enabled = false;
                buttonNextStep.Enabled = false;
                buttonAddNewJob.Enabled = true;

                query = "";




                Stream myStream = File.Open(tempJob + @"\1_job.bmp",FileMode.Open);
                byte[] rawByte1 = new byte[myStream.Length];
                myStream.Read(rawByte1, 0, Convert.ToInt32(myStream.Length));
                string photoFromScaps = "0x";
                photoFromScaps += ByteArrayToString(rawByte1);

                myStream.Close();



                myStream = File.Open(tempJob + @"\1_job.sjf", FileMode.Open);
                byte[] rawByte2 = new byte[myStream.Length];
                myStream.Read(rawByte2, 0, Convert.ToInt32(myStream.Length));
                string saveFromScaps = "0x";
                saveFromScaps += ByteArrayToString(rawByte2);
                
                myStream.Close();



                myStream = File.Open(tempJob + @"\1_job.xml", FileMode.Open);
                byte[] rawByte3 = new byte[myStream.Length];
                myStream.Read(rawByte3, 0, Convert.ToInt32(myStream.Length));
                string xmlFromScaps = "0x";
                xmlFromScaps += ByteArrayToString(rawByte3);

                myStream.Close();



                if (File.Exists(tempJob + @"\1_cam.bmp"))
                    myStream = File.Open(tempJob + @"\1_cam.bmp", FileMode.Open);
                else
                    myStream = File.Open(mainFolder + @"\default.bmp", FileMode.Open);

                byte[] rawByte4 = new byte[myStream.Length];
                myStream.Read(rawByte4, 0, Convert.ToInt32(myStream.Length));
                string imageFromCamera = "0x";
                imageFromCamera += ByteArrayToString(rawByte4);

                myStream.Close();



                query += "INSERT INTO `jobautostack`(`ID`, `photoFromScaps`, `saveFromScaps`, `xmlFromScaps`, `imageFromCamera`)";
                query += "VALUES (";
                query += iDLabel1.Text + ", ";
                query += photoFromScaps + ", ";
                query += saveFromScaps + ", ";
                query += xmlFromScaps + ", ";
                query += imageFromCamera + ")";

                runDBQueryReturnVal(query);
            }

            allTextBoxClear();

        jobDBRefresh();
        }

        
        
        /////////////////////////////////////////////////////////////////////////////////////////////////




        private void runDBQueryReturnFile(string query, string nameOfFile)
        {
            if (query == "")
            {
                MessageBox.Show("brak zapytania MySQL");
            }

            MySqlConnection connectionDB = new MySqlConnection(mySQLConnectionString);

            MySqlCommand commandDB = new MySqlCommand(query, connectionDB);

            commandDB.CommandTimeout = 60;

            try
            {
                connectionDB.Open();

                MySqlDataReader myReader = commandDB.ExecuteReader();


                if (myReader != null)
                    {
                        int i = 1;
                        while (myReader.Read())
                        {
                            var blob = new Byte[(myReader.GetBytes(0, 0, null, 0, int.MaxValue))];
                            myReader.GetBytes(0, 0, blob, 0, blob.Length);
                            using (var fs = new FileStream(tempJob + @"\" + i.ToString() + "_" + nameOfFile, FileMode.Create, FileAccess.Write))
                                fs.Write(blob, 0, blob.Length);
                            i++;
                        }
                    }


            }
            catch (Exception ex)
            {
                MessageBox.Show("błąd zapytania " + ex.Message);
            }

        }



        Bitmap img;

        private void refreshImageAndFile()
        {

            pictureBox1.ImageLocation = mainFolder + @"\load.bmp";

            System.IO.DirectoryInfo di = new DirectoryInfo(tempJob);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            string actualID = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["ID"].Value.ToString();

            string query = "SELECT `photofromscaps` FROM `jobautostack` WHERE ID = " + actualID;

            runDBQueryReturnFile(query, "job.bmp");


            if (File.Exists(tempJob + @"\1_job.bmp"))
            {
                img = new Bitmap(tempJob + @"\1_job.bmp");
                labelDimension.Text = "Rozmiar detalu: " + dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["dimension"].Value.ToString();
                img.Dispose();

                pictureBox1.ImageLocation = tempJob + @"\1_job.bmp";


              //  pictureBox1.ImageLocation = tempJob + @"\1_job.bmp";
            }
            else
                pictureBox1.ImageLocation = mainFolder + @"\default.bmp";


        }

        private void buttonAddNewJob_Click(object sender, EventArgs e)
        {
            buttonAddFileToThisJob.Enabled = true;
            buttonFinishNewJob.Enabled = true;
            buttonNextStep.Enabled = false;
            buttonAddNewJob.Enabled = false;

            textBoxListOfFileInThisProject.Text = "";
            int numerid = 1 + Convert.ToInt16(runDBQueryReturnVal("SELECT MAX(ID) FROM `job`"));
            iDLabel1.Text = numerid.ToString();
            allTextBoxEnable();

            dateStartDateTimePicker.Value = DateTime.Now;

        }

        private void allTextBoxDisable()
        {
            dateStartDateTimePicker.Enabled = false;
            dateStopDateTimePicker.Enabled = false;
            orderCompanyComboBox.Enabled = false;
            orderNumberTextBox.Enabled = false;
            pieceMaterialComboBox.Enabled = false;
            jobTypeComboBox.Enabled = false;
            jobDepthComboBox.Enabled = false;
            countPcsTextBox.Enabled = false;
            lensTypeComboBox.Enabled = false;
            countLayerTextBox.Enabled = false;
            surfaceDimensionTextBox.Enabled = false;
            hatch1TypeTextBox.Enabled = false;
            hatch2TypeTextBox.Enabled = false;
            timeOfJobTextBox.Enabled = false;
            timeOfJobRealTextBox.Enabled = false;
            positionZTextBox.Enabled = false;
            commentTextBox.Enabled = false;
            DimensionTextBox.Enabled = false;

        }


        private void allTextBoxEnable()
        {
            dateStartDateTimePicker.Enabled = true;
            dateStopDateTimePicker.Enabled = true;
            orderCompanyComboBox.Enabled = true;
            orderNumberTextBox.Enabled = true;
            pieceMaterialComboBox.Enabled = true;
            jobTypeComboBox.Enabled = true;
            jobDepthComboBox.Enabled = true;
            countPcsTextBox.Enabled = true;
            lensTypeComboBox.Enabled = true;
            countLayerTextBox.Enabled = true;
            surfaceDimensionTextBox.Enabled = true;
            hatch1TypeTextBox.Enabled = true;
            hatch2TypeTextBox.Enabled = true;
            timeOfJobTextBox.Enabled = true;
            timeOfJobRealTextBox.Enabled = true;
            positionZTextBox.Enabled = true;
            commentTextBox.Enabled = true;
            DimensionTextBox.Enabled = true;

        }
        private void allTextBoxClear()
        {
            orderCompanyComboBox.Text = "";
            orderNumberTextBox.Text = "";
            pieceMaterialComboBox.Text = "";
            jobTypeComboBox.Text = "";
            jobDepthComboBox.Text = "";
            countPcsTextBox.Text = "";
            lensTypeComboBox.Text = "";
            countLayerTextBox.Text = "";
            surfaceDimensionTextBox.Text = "";
            hatch1TypeTextBox.Text = "";
            hatch2TypeTextBox.Text = "";
            timeOfJobTextBox.Text = "";
            timeOfJobRealTextBox.Text = "";
            positionZTextBox.Text = "";
            commentTextBox.Text = "";
            DimensionTextBox.Text = "";

        }

        string mainFolder = @"C:\engraver\database";
        string tempJob = @"C:\engraver\database\temp";


        private void button1_Click(object sender, EventArgs e)
        {

            buttonAddFileToThisJob.Enabled = true;
            buttonFinishNewJob.Enabled = true;
            buttonNextStep.Enabled = true;
            buttonAddNewJob.Enabled = false;


            if (!(Directory.Exists(tempJob))) 
            {
                Directory.CreateDirectory(tempJob);
            }

            dateStopDateTimePicker.Value = DateTime.Now;

            generatePhotoFromScaps();
            generateSaveFromScaps();
            generateXmlFromScaps();
            //generateImageFromCamera();

            Bitmap bmp = new Bitmap(tempJob + @"\1_job.bmp");
            var black = 0;
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    Color color = bmp.GetPixel(x, y);

                    if (bmp.GetPixel(x, y).ToArgb().Equals(Color.Black.ToArgb()))
                    {
                        black++;
                    }
                }
            }

            black = black / 250;
            if (black < 10000)
            {
                surfaceDimensionTextBox.Text = Convert.ToString(black + " mm²");
            }
            else
            {

                surfaceDimensionTextBox.Text = Convert.ToString(black / 100 + " cm²");
            }


            bmp.Dispose();

            axScSamlightClientCtrl1.ScSetLongValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlLongValueTypeMotionAxis, 2);
           // positionZTextBox.Text = Convert.ToString( axScSamlightClientCtrl1.ScExecCommand((int)ScComSAMLightClientCtrlExecCommandConstants.scComSAMLightClientCtrlExecCommandMotionUpdatePos));

        }



        class SAMComment
        {
            public string typeOfComment;
            public string comment;

            public SAMComment()
            {
            }

            public SAMComment(string _typeOfComment, string _comment)
            {
                typeOfComment = _typeOfComment;
                comment = _comment;
            }

            public SAMComment(string _comment)
            {
                typeOfComment = "download from SAM";
                comment = _comment;
            }
        }

        class SAMPen
        {
            public int numerPen;
            public double laserPower;
            public double markSpeed;
            public double frequency;

            public int waveForm;
            public double simmerCurrent;

            public double pulseLenght;
            public double firstPulseLenght;

            public bool wobbleEnable;
            public double wobbleFreq;
            public double wobbleAmpl;


            public double jumpSpeed;
            public double jumpDelay;
            public double markDelay;
            public double polyDelay;
            public double laserOnDelay;
            public double laserOffDelay;
            /*korekcja ścieżki
             hatch1
             * hatch2 (kąt, gęstość, tryb)
             * grubość plamki
             */
            public SAMPen()
            {
            }
        }

        class wannaPen
        {
            public bool wanna;
            public wannaPen(bool _wanna)
            {
                wanna = _wanna;
            }
        }


        List<SAMPen> listOfPen = new List<SAMPen>();
        List<SAMComment> listOfComment = new List<SAMComment>();
        List<wannaPen> wannaPenList = new List<wannaPen>();

        private void generateXmlFromScaps()
        {
            listOfPen.Clear();
            listOfComment.Clear();
            wannaPenList.Clear();

            importEntityFromSAM();
            importParamFromSAM();

            SaveSAMParamAsXml();
        }



        private void SaveSAMParamAsXml()
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(tempJob + @"\1_job.xml"))
            {
                file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

                file.WriteLine("<listOfPen>");

                foreach (SAMPen actPen in listOfPen)
                {
                    file.WriteLine("    <pen>");

                    file.WriteLine("        <numerPen>" + Convert.ToString(actPen.numerPen) + "</numerPen>");
                    file.WriteLine("        <laserPower>" + Convert.ToString(actPen.laserPower) + "</laserPower>");
                    file.WriteLine("        <markSpeed>" + Convert.ToString(actPen.markSpeed) + "</markSpeed>");
                    file.WriteLine("        <frequency>" + Convert.ToString(actPen.frequency) + "</frequency>");
                    file.WriteLine("        <waveForm>" + Convert.ToString(actPen.waveForm) + "</waveForm>");
                    file.WriteLine("        <simmerCurrent>" + Convert.ToString(actPen.simmerCurrent) + "</simmerCurrent>");
                    file.WriteLine("        <pulseLenght>" + Convert.ToString(actPen.pulseLenght) + "</pulseLenght>");
                    file.WriteLine("        <firstPulseLenght>" + Convert.ToString(actPen.firstPulseLenght) + "</firstPulseLenght>");
                    file.WriteLine("        <wobbleEnable>" + Convert.ToString(actPen.wobbleEnable) + "</wobbleEnable>");
                    file.WriteLine("        <wobbleFreq>" + Convert.ToString(actPen.wobbleFreq) + "</wobbleFreq>");
                    file.WriteLine("        <wobbleAmpl>" + Convert.ToString(actPen.wobbleAmpl) + "</wobbleAmpl>");
                    file.WriteLine("        <jumpSpeed>" + Convert.ToString(actPen.jumpSpeed) + "</jumpSpeed>");
                    file.WriteLine("        <jumpDelay>" + Convert.ToString(actPen.jumpDelay) + "</jumpDelay>");
                    file.WriteLine("        <markDelay>" + Convert.ToString(actPen.markDelay) + "</markDelay>");
                    file.WriteLine("        <polyDelay>" + Convert.ToString(actPen.polyDelay) + "</polyDelay>");
                    file.WriteLine("        <laserOnDelay>" + Convert.ToString(actPen.laserOnDelay) + "</laserOnDelay>");
                    file.WriteLine("        <laserOffDelay>" + Convert.ToString(actPen.laserOffDelay) + "</laserOffDelay>");

                    file.WriteLine("    </pen>");

                }

                foreach (SAMComment actComment in listOfComment)
                {
                        file.WriteLine("    <comment>");

                        file.WriteLine("        <comment>" + actComment.comment + "</comment>");
                        file.WriteLine("        <commenttype>" + actComment.typeOfComment + "</commenttype>");

                        file.WriteLine("    </comment>");
                }


                file.WriteLine("</listOfPen>");

            }
        }


        private void importEntityFromSAM()
        {

            string entityName = "", entityType = "", entityPen = "";
            string hatch1Enable = "", hatch1Angle = "";
            string hatch1Dimension = "", hatch1Style = "";
            string hatch2Enable = "", hatch2Angle = "";
            string hatch2Dimension = "", hatch2Style = "";

            string markHatch = "", markContour = "", markLoop = "";

            bool firstEntity = true;

            for (int tempfor = 0; tempfor <= 255; tempfor++)
            {
                wannaPenList.Add(new wannaPen(false));
            }

            long temp = 0;
            double tempdouble = 0;

            int layerCount = 0;
            int i, count;

            //m_samlight.ScSetMode(0);

            axScSamlightClientCtrl1.ScSetMode((int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlModeFlagTopLevelOnly);

            count = axScSamlightClientCtrl1.ScGetLongValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlLongValueTypeTopLevelEntityNum);

            if (count > 0)
            {

                for (i = 0; i < count; i++)
                {
                    axScSamlightClientCtrl1.ScGetIDStringData((int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlStringDataIdGetToplevelEntity, i, ref entityName);

                    if (entityName == "")
                    {
                        entityName = "'empty'";
                    }

                    axScSamlightClientCtrl1.ScGetIDStringData((int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlStringDataIdGetEntityType, i, ref entityType);

                    int loopCountInt = axScSamlightClientCtrl1.ScGetEntityLongData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlLongDataIdEntityMarkLoopCount);
                    markLoop = Convert.ToString(loopCountInt);

                    ////wyciągnięcie informacji o hatch 1

                    temp = axScSamlightClientCtrl1.ScGetEntityLongData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlLongDataIdEnableHatching1);
                    if (temp > 0)
                    {
                        hatch1Enable = "Włączony";
                        layerCount += loopCountInt;
                    }

                    

                    switch (temp)
                    {
                        case 1: hatch1Style = "1.naprzemiennie bez skoków"; break;
                        case 2: hatch1Style = "2.lewo-prawo bez skoków"; break;
                        case 3: hatch1Style = "3.prawo-lewo bez skoków"; break;
                        case 4: hatch1Style = "4.dookoła"; break;
                        case 5: hatch1Style = "5.naprzemienne ze skokami"; break;
                        case 6: hatch1Style = "6.zigzag"; break;
                        default: hatch1Style = "0.brak"; hatch1Enable = "Wyłączony"; break;
                    };

                    axScSamlightClientCtrl1.ScGetEntityDoubleData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlDoubleDataIdHatchDistance1, ref tempdouble);
                    hatch1Dimension = Convert.ToString(tempdouble);

                    axScSamlightClientCtrl1.ScGetEntityDoubleData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlDoubleDataIdHatchAngle1, ref tempdouble);
                    hatch1Angle = Convert.ToString(tempdouble * (180.0 / Math.PI));



                    ////wyciągnięcie informacji o hatch 2

                    temp = axScSamlightClientCtrl1.ScGetEntityLongData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlLongDataIdEnableHatching2);
                    if (temp > 0)
                    {
                        hatch2Enable = "Włączony";
                        layerCount += loopCountInt;
                    }

                    switch (temp)
                    {
                        case 1: hatch2Style = "1.naprzemiennie bez skoków"; break;
                        case 2: hatch2Style = "2.lewo-prawo bez skoków"; break;
                        case 3: hatch2Style = "3.prawo-lewo bez skoków"; break;
                        case 4: hatch2Style = "4.dookoła"; break;
                        case 5: hatch2Style = "5.naprzemienne ze skokami"; break;
                        case 6: hatch2Style = "6.zigzag"; break;
                        default: hatch2Style = "0.brak"; hatch2Enable = "Wyłączony"; break;
                    };

                    axScSamlightClientCtrl1.ScGetEntityDoubleData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlDoubleDataIdHatchDistance2, ref tempdouble);
                    hatch2Dimension = Convert.ToString(tempdouble);

                    axScSamlightClientCtrl1.ScGetEntityDoubleData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlDoubleDataIdHatchAngle2, ref tempdouble);
                    hatch2Angle = Convert.ToString(tempdouble * (180.0 / Math.PI));

                    //pobranie pozostałych informacji

                    temp = axScSamlightClientCtrl1.ScGetEntityLongData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlLongDataIdEntityMarkFlags);

                    switch (temp)
                    {
                        case 1: markContour = "TAK"; markHatch = "nie"; break;
                        case 2: markContour = "nie"; markHatch = "TAK"; break;
                        case 3: markContour = "TAK"; markHatch = "TAK"; break;
                        default: markContour = "nie"; markHatch = "nie"; break;
                    };



                    //pobranie używanego pena. zapisa wartości do listy penów (do wykorzystania później)

                    temp = axScSamlightClientCtrl1.ScGetEntityLongData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlLongDataIdEntityGetPen);

                    wannaPenList[Convert.ToInt16(temp)].wanna = true;
                    entityPen = Convert.ToString(temp);

                    temp = axScSamlightClientCtrl1.ScGetEntityLongData(entityName, (int)ScComSAMLightClientCtrlFlags.scComSAMLightClientCtrlLongDataIdEntityMarkLoopCount);

                    //zapis do pamięci

                    listOfComment.Add(new SAMComment("- entity nr: " + (i + 1) + ", nazwa: " + entityName));
                    listOfComment.Add(new SAMComment("      typ: " + entityType + ", użyty pen: " + entityPen));
                    
                    listOfComment.Add(new SAMComment(""));
                    listOfComment.Add(new SAMComment("    - kontur: " + markContour + ", hatch: " + markHatch + ", ilość warstw: " + markLoop));

                    listOfComment.Add(new SAMComment("    - hatch1: " + hatch1Enable + ", kąt: " + hatch1Angle));
                    listOfComment.Add(new SAMComment("      dystans: " + hatch1Dimension + ", styl: " + hatch1Style));

                    listOfComment.Add(new SAMComment("    - hatch2: " + hatch2Enable + ", kąt: " + hatch2Angle));
                    listOfComment.Add(new SAMComment("      dystans: " + hatch2Dimension + ", styl: " + hatch2Style));
                    listOfComment.Add(new SAMComment(""));

                    if (firstEntity)
                    {
                        if (hatch1Enable != "Wyłączony")
                        {
                            hatch1TypeTextBox.Text = "dyst: " + hatch1Dimension + ", styl: " + hatch1Style;
                        }
                        else
                        {
                            hatch1TypeTextBox.Text = "brak";
                        }
                        if (hatch2Enable != "Wyłączony")
                        {
                            hatch2TypeTextBox.Text = "dyst: " + hatch2Dimension + ", styl: " + hatch2Style;
                        }
                        else
                        {
                            hatch2TypeTextBox.Text = "brak";
                        }

                        firstEntity = false;
                    }
                }
                countLayerTextBox.Text = Convert.ToString( layerCount);

            }

            else listOfComment.Add(new SAMComment("brak entity. Sprawdź SCAPS SAMLight"));


        }

        private void importParamFromSAM()
        {
            /*
            listBoxParam.Items.Clear();

            int pierwszyPen, aktualnyPen, ostatniPen;
            try
            {
                pierwszyPen = System.Convert.ToInt16(textBoxFrom.Text);
                if (pierwszyPen < 1)
                {
                    textBoxFrom.Text = "1";
                    pierwszyPen = 1;
                }
                if (pierwszyPen > 255)
                {
                    textBoxFrom.Text = "255";
                    pierwszyPen = 255;
                }
            }

            catch (Exception)
            {
                textBoxFrom.Text = "1";
                pierwszyPen = 1;
            }
            

            try
            {
                ostatniPen = System.Convert.ToInt16(textBoxTo.Text);
                if (ostatniPen < 1)
                {
                    textBoxTo.Text = "1";
                    ostatniPen = 1;
                }
                if (ostatniPen > 255)
                {
                    textBoxTo.Text = "255";
                    ostatniPen = 255;
                }
                if (ostatniPen < pierwszyPen)
                {
                    textBoxTo.Text = textBoxFrom.Text;
                    ostatniPen = pierwszyPen;
                }
            }

            catch (Exception)
            {
                textBoxTo.Text = textBoxFrom.Text;
                ostatniPen = pierwszyPen;
            }


            */

            // listOfFile.Add(new SAMFile());
            try
            {
                double czasPracy = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeLastExpectedMarkTime);

                if (czasPracy < 100)
                {
                    timeOfJobTextBox.Text = Convert.ToString(Math.Ceiling(czasPracy) + " sek");
                }
                else
                {

                    timeOfJobTextBox.Text = Convert.ToString(Math.Ceiling(czasPracy / 60) + " min");
                }

                listOfComment.Add(new SAMComment("Szacowany czas pracy: " + timeOfJobTextBox.Text));



                czasPracy = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeLastMarkTime);

                if (czasPracy < 100)
                {
                    timeOfJobRealTextBox.Text = Convert.ToString(Math.Ceiling(czasPracy) + " sek");
                }
                else
                {

                    timeOfJobRealTextBox.Text = Convert.ToString(Math.Ceiling(czasPracy / 60) + " min");
                }

                listOfComment.Add(new SAMComment("Rzeczywisty czas pracy: " + timeOfJobTextBox.Text));

                lensTypeComboBox.Text = Convert.ToString(2 * axScSamlightClientCtrl1.ScGetWorkingArea(4));

                listOfComment.Add(new SAMComment("Użyta soczewka: " + lensTypeComboBox.Text));

                DimensionTextBox.Text = Convert.ToString(Math.Round(axScSamlightClientCtrl1.ScGetEntityOutline("", 3) - axScSamlightClientCtrl1.ScGetEntityOutline("", 0),1));
                DimensionTextBox.Text += " x ";
                DimensionTextBox.Text += Convert.ToString(Math.Round(axScSamlightClientCtrl1.ScGetEntityOutline("", 4) - axScSamlightClientCtrl1.ScGetEntityOutline("", 1),1));
                DimensionTextBox.Text += " mm";

                listOfComment.Add(new SAMComment("Rozmiar: " + DimensionTextBox.Text));
            }
            catch
            {
            }

            for (int aktualnyPen = 0; aktualnyPen <= 255; aktualnyPen++)
            {
                if (wannaPenList[aktualnyPen].wanna == true)
                {
                    listOfPen.Add(new SAMPen());

                    axScSamlightClientCtrl1.ScSetPen(aktualnyPen);
                    int i = listOfPen.Count - 1;

                    listOfPen[i].numerPen = aktualnyPen;
                    listOfPen[i].laserPower = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeLaserPower);
                    listOfPen[i].markSpeed = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeMarkSpeed);
                    listOfPen[i].frequency = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeFrequency);

                    listOfPen[i].waveForm = axScSamlightClientCtrl1.ScGetLongValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlLongValueTypeSpiG3Waveform);
                    listOfPen[i].simmerCurrent = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeSpiLaserSimmer);

                    listOfPen[i].pulseLenght = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypePulseLength);
                    listOfPen[i].firstPulseLenght = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeFirstPulseLength);

                    long temp1 = axScSamlightClientCtrl1.ScGetLongValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlLongValueTypeDeviceEnableFlagsValue);

                    if (((temp1 % 16) - (temp1 % 8)) == 0)
                    {
                        listOfPen[i].wobbleEnable = false;
                    }
                    else
                    {
                        listOfPen[i].wobbleEnable = true;
                    }

                    listOfPen[i].wobbleFreq = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeWobbleFrequency);
                    listOfPen[i].wobbleAmpl = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeWobbleAmplitude);
                    listOfPen[i].firstPulseLenght = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeFirstPulseLength);

                    listOfPen[i].jumpSpeed = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeJumpSpeed);
                    listOfPen[i].jumpDelay = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeJumpDelay);
                    listOfPen[i].markDelay = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeMarkDelay);
                    listOfPen[i].polyDelay = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypePolyDelay);
                    listOfPen[i].laserOnDelay = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeLaserOnDelay);
                    listOfPen[i].laserOffDelay = axScSamlightClientCtrl1.ScGetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeLaserOffDelay);

                }
            }
        }

        private void generateSaveFromScaps()
        {
            axScSamlightClientCtrl1.ScSaveJob(tempJob + @"\1_job.sjf", 3);
        }

        private void generatePhotoFromScaps()
        {

            axScSamlightClientCtrl1.ScSetStringValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlStringValueTypeSaveLayerAdjustableDPI, tempJob + @"\1_job.bmp");

            /*
            axScSamlightClientCtrl1.ScSetStringValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlStringValueTypeSaveView2D160, @"C:\impex\v2d160.bmp");
            axScSamlightClientCtrl1.ScSetStringValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlStringValueTypeSaveView2D320, @"C:\impex\v2d320.bmp");
            axScSamlightClientCtrl1.ScSetStringValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlStringValueTypeSaveView2DVariableSize, @"C:\impex\v2dvar.bmp");
            axScSamlightClientCtrl1.ScSetStringValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlStringValueTypeSaveView2DFull, @"C:\impex\v2dfull.bmp");
            */

        }

        private void filePathLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }

        private byte[] imageToByte(System.Drawing.Image img)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                return ms.ToArray();
            }
        }

        private void buttonAddFileToThisJob_Click(object sender, EventArgs e)
        {
            string newID = Convert.ToString( 1 + Convert.ToInt32( runDBQueryReturnVal("SELECT MAX(ID) FROM `jobfilestack`")));


            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\" ;
            //openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*" ;
            openFileDialog1.FilterIndex = 2 ;
            openFileDialog1.RestoreDirectory = true ;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            //int FileSize = Convert.ToInt32(myStream.Length);
                            //Image newimg =  myStream;
                            
                            //byte[] rawData = imageToByte(myStream);

                            //byte[] encData_byte = new byte[myStream.Length];
                            //myStream.Read(encData_byte, 0, Convert.ToInt32( myStream.Length));
                            //string encodedData = "0x";
                            //    encodedData += Convert.ToBase64String(encData_byte);


                           // myStream.Read(rawData, 0, FileSize);

                            //textBox1.Text += rawData.ToString();

                            string fileName = openFileDialog1.SafeFileName;

                            byte[] rawByte = new byte[myStream.Length];

                            myStream.Read(rawByte, 0, Convert.ToInt32(myStream.Length));

                            string rawData = "0x";
                            rawData += ByteArrayToString(rawByte);

                            string query = "";
                            query += "INSERT INTO `jobfilestack`(`ID`, `jobID`, `fileName`, `fileSize`, `file`, `comment`)";
                            query += "VALUES(";
                            query += newID + ", \"";                        //ID
                            query += iDLabel1.Text + "\", \"";              //jobID
                            query += fileName + "\", \"";                   //fileName
                            query += myStream.Length + "\", ";            //fileSize
                            query += rawData + ", \"";                    //file
                            query += "X" + "\")";                           //comment

                            runDBQueryReturnVal(query);

                            textBoxListOfFileInThisProject.Text += fileName + "\r\n";

                            myStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }

        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        int lastIDIndex = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(dataGridDB.RowCount > 0)
                if (dataGridDB.CurrentCell.RowIndex > -1)
                    if (lastIDIndex != dataGridDB.CurrentCell.RowIndex)
                    {
                        lastIDIndex = dataGridDB.CurrentCell.RowIndex;
                        refreshImageAndFile();

                    }
         /*   if (lastIDIndex != dataGridDB.CurrentRow.Index)
            {
                lastIDIndex = dataGridDB.CurrentRow.Index;
                refreshImageAndFile();

            }
            */
        }

        private void buttonOpenFolderWithThisJob_Click(object sender, EventArgs e)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(tempJob);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            string actualID = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["ID"].Value.ToString();


            string query = "SELECT `photofromscaps` FROM `jobautostack` WHERE ID = " + actualID;
            runDBQueryReturnFile(query, "job.bmp");

            query = "SELECT `savefromscaps` FROM `jobautostack` WHERE ID = " + actualID;
            runDBQueryReturnFile(query, "job.sjf");

            query = "SELECT `xmlfromscaps` FROM `jobautostack` WHERE ID = " + actualID;
            runDBQueryReturnFile(query, "job.xml");

            query = "SELECT `imagefromcamera` FROM `jobautostack` WHERE ID = " + actualID;
            runDBQueryReturnFile(query, "cam.bmp");

            System.Diagnostics.Process.Start(tempJob);

        }

        private void buttonReadDataFromTableAndWriteThisToNewJob_Click(object sender, EventArgs e)
        {
            orderCompanyComboBox.Text = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["orderCompany"].Value.ToString();
            orderNumberTextBox.Text = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["orderID"].Value.ToString();
            pieceMaterialComboBox.Text = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["material"].Value.ToString();
            jobTypeComboBox.Text = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["jobType"].Value.ToString();
            jobDepthComboBox.Text = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["jobDepth"].Value.ToString();
            lensTypeComboBox.Text = dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["lensType"].Value.ToString();

        }

        private void toolStripButtonSetting_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<FormSetting>().Count() == 0)
            {
                FormSetting frm_setting = new FormSetting();
                frm_setting.Show();
            }
        }

        private void toolStripButtonEditOneJob_Click(object sender, EventArgs e)
        {

            MySqlConnection connectionDB = new MySqlConnection(mySQLConnectionString);

            MySqlCommand commandDB = new MySqlCommand("SELECT * FROM `job` WHERE ID=" + dataGridDB.Rows[dataGridDB.CurrentRow.Index].Cells["ID"].Value.ToString(), connectionDB);

            commandDB.CommandTimeout = 60;

            try
            {
                connectionDB.Open();

                MySqlDataReader myReader = commandDB.ExecuteReader();

                if (myReader.HasRows)
                {
                    while (myReader.Read())
                    {
                        
                        if (Application.OpenForms.OfType<FormEditJob>().Count() == 0)
                        {

                            FormEditJob FormEditJob_1 = new FormEditJob(  myReader.GetString(0),
                                                                        myReader.GetString(1),
                                                                        myReader.GetString(2),
                                                                        myReader.GetString(3),
                                                                        myReader.GetString(4),
                                                                        myReader.GetString(5),
                                                                        myReader.GetString(6),
                                                                        myReader.GetString(7),
                                                                        myReader.GetString(8),
                                                                        myReader.GetString(9),
                                                                        myReader.GetString(10),
                                                                        myReader.GetString(11),
                                                                        myReader.GetString(12),
                                                                        myReader.GetString(13),
                                                                        myReader.GetString(14),
                                                                        myReader.GetString(15),
                                                                        myReader.GetString(16),
                                                                        myReader.GetString(17),
                                                                        myReader.GetString(18),
                                                                        myReader.GetString(19));
                            if (FormEditJob_1.ShowDialog() == DialogResult.OK)
                            {



                                runDBQueryReturnVal("INSERT INTO `historyeditjob` (`ID`, `user`, `date`, `history`) VALUES (NULL, '" +
                                                        user + "', '" +
                                                        DateTime.Now + "', 'Change data job; ID=" +
                                                        FormEditJob_1.queryListOfChange + "')");

                                runDBQueryReturnVal(FormEditJob_1.returnAfterText);




                                runDBQueryReturnToDataGrid("SELECT * FROM `job`");
                            }
                        }

                       

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("błąd zapytania " + ex.Message);
            }

        }

        private void buttonSearchCompany_Click(object sender, EventArgs e)
        {
            searchUsingSomeTB();
        }

        private void searchUsingSomeTB()
        {
            try
            {
                bool multiquery = false;
                string query = "SELECT * FROM `job` WHERE ";

                if ((textBoxIDfrom.Text != "") || (textBoxIDto.Text != ""))
                {
                    if ((textBoxIDfrom.Text != "") && (textBoxIDto.Text != ""))
                    {
                        query += "`ID` BETWEEN " + textBoxIDfrom.Text + " AND " + textBoxIDto.Text + " ";
                    }
                    else
                    {
                        if (textBoxIDfrom.Text != "")
                            query += "`ID` >= " + textBoxIDfrom.Text + " ";
                        if (textBoxIDto.Text != "")
                            query += "`ID` <= " + textBoxIDto.Text + " ";
                    }

                    multiquery = true;
                }

                if (findCompanyComboBox.Text != "")
                {
                    if (multiquery) query += "AND ";
                    query += "`orderCompany` LIKE '%" + findCompanyComboBox.Text + "%' ";
                    multiquery = true;
                }


                if (findCompanyIDTextBox.Text != "")
                {
                    if (multiquery) query += "AND ";
                    query += "`orderid` LIKE '%" + findCompanyIDTextBox.Text + "%' ";
                    multiquery = true;
                }


                if (findMaterialComboBox.Text != "")
                {
                    if (multiquery) query += "AND ";
                    query += "`material` LIKE '%" + findMaterialComboBox.Text + "%' ";
                    multiquery = true;
                }


                if (findJobTypeComboBox.Text != "")
                {
                    if (multiquery) query += "AND ";
                    query += "`jobType` LIKE '%" + findJobTypeComboBox.Text + "%' ";
                    multiquery = true;
                }


                if (findJobDepthComboBox.Text != "")
                {
                    if (multiquery) query += "AND ";
                    query += "`jobDepth` LIKE '%" + findJobDepthComboBox.Text + "%' ";
                    multiquery = true;
                }


                if (findLensTypeComboBox.Text != "")
                {
                    if (multiquery) query += "AND ";
                    query += "`lensType` LIKE '%" + findLensTypeComboBox.Text + "%' ";
                    multiquery = true;
                }

                runDBQueryReturnToDataGrid(query);
            }
            catch(Exception ex)
            {
                MessageBox.Show("brak danych " + ex.Message);

                jobDBRefreshFunction();
            }

        }

        private void buttonFindDateStart_Click(object sender, EventArgs e)
        {
            findDateStart();
        }

        private void findDateStart()
        {
            
            runDBQueryReturnToDataGrid("SELECT * FROM `job` WHERE `datestart` LIKE '%" + textBoxFindDateStart.Text + "%'");
        }

        private void buttonFindDateStop_Click(object sender, EventArgs e)
        {
            findDateStop();
        }

        private void findDateStop()
        {
            runDBQueryReturnToDataGrid("SELECT * FROM `job` WHERE `datestop` LIKE '%" + textBoxFindDateStop.Text + "%'");

        }

        private void toolStripButtonChangeLogin_Click(object sender, EventArgs e)
        {
            loginMenu();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<FormPrint>().Count() == 0)
            {
                FormPrint FormPrint_1 = new FormPrint();
                FormPrint_1.Show();
            }

        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            myWebBrowser.Print();
        }

        private void buttonShowPrintDialog_Click(object sender, EventArgs e)
        {
            myWebBrowser.ShowPrintDialog();
        }

        private void buttonSetupPage_Click(object sender, EventArgs e)
        {
            myWebBrowser.ShowPageSetupDialog();
        }

        private void buttonPrintPreview_Click(object sender, EventArgs e)
        {
            myWebBrowser.ShowPrintPreviewDialog();

        }

        private void buttonGenrePrintableFileFromGrid_Click(object sender, EventArgs e)
        {
            labelPrintReady.Text = "Generuję dokument...";

            genreAndSaveHtml();

            labelPrintReady.Text = "Pobieram pliki z bazy danych...";

            loadJobGraph();

            labelPrintReady.Text = "Otwieram plik do druku...";

            openHtml();

            labelPrintReady.Text = "GOTOWY DO DRUKU!";
        }
        
        private void loadJobGraph()
        {

            for (int i = 0; i < 12; i++)
            {
                if (i < dataGridDB.RowCount)
                {
                    string actualID = Convert.ToString(dataGridDB.Rows[i].Cells["ID"].Value);

                    string query = "SELECT `photofromscaps` FROM `jobautostack` WHERE ID = " + actualID;

                    runDBQueryReturnFile(query, "job_"+actualID+".bmp");
                }
            }
        }


        WebBrowser myWebBrowser = new WebBrowser();

        private void genreAndSaveHtml()
        {

            System.IO.DirectoryInfo di = new DirectoryInfo(mainFolder + @"\temp");

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(mainFolder + @"\temp\print.html"))
            {
                file.WriteLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                file.WriteLine("<html lang=\"pl-PL\" xml:lang=\"pl\">");
                file.WriteLine("<head>");
                file.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
                file.WriteLine("");
                file.WriteLine("<style type=\"text/css\" media=\"all\">");

                file.WriteLine("");
                file.WriteLine("table {");
                file.WriteLine("    border-collapse: collapse;");
                file.WriteLine("    width: 100%;");
                file.WriteLine("    border: 1px solid #ddd;");
                file.WriteLine("}");
                file.WriteLine("");
                file.WriteLine("th, td {");
                file.WriteLine("    padding: 2px;");
                file.WriteLine("    text-align: left;");
                file.WriteLine("    border-bottom: 1px solid #ddd;");
                file.WriteLine("}");
                file.WriteLine("");
                file.WriteLine("</style>");
                file.WriteLine("</head>");
                file.WriteLine("<body>");
                file.WriteLine("<p align=center><b>Eksport zleceń wykonanych</b></p>");
                file.WriteLine("<p align=center>Wygenerowano dnia " + DateTime.Now + " przez: "+mySQLUserName+"</p>");
                file.WriteLine("<p align=right>");
                file.WriteLine("<table style=\"width:18cm\">");
                file.WriteLine("  <tr>");
                file.WriteLine("    <td><b>NR</b></td>");
                file.WriteLine("    <td><b>Data</b></td>");
                file.WriteLine("    <td><b>Zleceniodawca(nr)</b></td>");
                file.WriteLine("    <td><b>Typ(głęb)</b></td>");
                file.WriteLine("    <td><b>Ilość</b></td>");
                file.WriteLine("	<td><b>Rozmiar</b></td>");
                file.WriteLine("    <td><b>Cena</b></td>");
                file.WriteLine("  </tr>");

                int pageCount = 0;

                for (int i = 0; i < 12; i++)
                {
                    if (i < dataGridDB.RowCount)
                    {
                        string cutDate = Convert.ToString(dataGridDB.Rows[i].Cells["dateStop"].Value);
                        cutDate = cutDate.Substring(0,10);

                        file.WriteLine("  <tr>");
                        file.WriteLine("    <td><b>" + dataGridDB.Rows[i].Cells["ID"].Value + "</b></td>");
                        file.WriteLine("    <td>" + cutDate + "</td>");
                        file.WriteLine("    <td>" + dataGridDB.Rows[i].Cells["orderCompany"].Value + "(" + dataGridDB.Rows[i].Cells["orderID"].Value + ")</td>");
                        file.WriteLine("    <td>" + dataGridDB.Rows[i].Cells["jobType"].Value + "(" + dataGridDB.Rows[i].Cells["jobDepth"].Value + ")</td>");
                        file.WriteLine("    <td>" + dataGridDB.Rows[i].Cells["countPcs"].Value + "</td>");
                        file.WriteLine("    <td>" + dataGridDB.Rows[i].Cells["surfaceDimension"].Value + "</td>");
                        file.WriteLine("	<td></td>");
                        file.WriteLine("  </tr>");
                    }
                }

                file.WriteLine("</table>");
                file.WriteLine("</p>");
                file.WriteLine("");


                for (int i = 0; i < 12; i++)
                {
                    if (i < dataGridDB.RowCount)
                    {
                        file.WriteLine("<p style=\"position:absolute;"+pos(i,0)+"\"><b>" + dataGridDB.Rows[i].Cells["ID"].Value + "</b></p>");
                        file.WriteLine("<p style=\"position:absolute;"+pos(i,1)+"\">" + dataGridDB.Rows[i].Cells["dimension"].Value + "</p>");
                        file.WriteLine("<img style=\"position:absolute;" + pos(i, 2) + ";max-height:4cm;max-width:4cm\" src=\"C:/engraver/database/temp/1_job_" + dataGridDB.Rows[i].Cells["ID"].Value + ".bmp\" />");
                    }
                }


                file.WriteLine("</body>");
                file.WriteLine("</html>");


            }


        }

        private string pos(int index,int type)
        {
            string leftPos = "";
            string topPos = "";

            if (index % 4 == 0) leftPos = "1";
            if (index % 4 == 1) leftPos = "5.5";
            if (index % 4 == 2) leftPos = "10";
            if (index % 4 == 3) leftPos = "14.5";

            int topPosTemp = 0;

            if (Math.Floor((double)index / 4) == 0) topPosTemp = 10;
            if (Math.Floor((double)index / 4) == 1) topPosTemp = 16;
            if (Math.Floor((double)index / 4) == 2) topPosTemp = 22;

            if (type == 2) topPosTemp++;

            topPos = topPosTemp.ToString();

            if (type == 1) topPos += ".5";

            return "left:"+leftPos+"cm;top:"+topPos+"cm";
        }

        string mySQLUserName;
        string mySQLUserPass;
        private void openHtml()
        {

            myWebBrowser.Navigate(@"C:\engraver\database\temp\print.html");
            // myWebBrowser.Show();
        }

        private void tabPageEditAndAdd_Click(object sender, EventArgs e)
        {

        }

        private void textBoxFindDateStartKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                findDateStart();
            }
        }

        private void textBoxFindDateStopKeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                findDateStop();
            }
        }

        private void showDBGridColumns(object sender, EventArgs e)
        {
            if (checkBoxdateStart.Checked)
            {
                dataGridDB.Columns["dateStart"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["dateStart"].Visible = false;
            } 
            
            if (checkBoxdateStop.Checked)
            {
                dataGridDB.Columns["dateStop"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["dateStop"].Visible = false;
            }

            if (checkBoxorderCompany.Checked)
            {
                dataGridDB.Columns["orderCompany"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["orderCompany"].Visible = false;
            }

            if (checkBoxorderID.Checked)
            {
                dataGridDB.Columns["orderID"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["orderID"].Visible = false;
            }

            if (checkBoxmaterial.Checked)
            {
                dataGridDB.Columns["material"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["material"].Visible = false;
            }

            if (checkBoxjobType.Checked)
            {
                dataGridDB.Columns["jobType"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["jobType"].Visible = false;
            }

            if (checkBoxjobDepth.Checked)
            {
                dataGridDB.Columns["jobDepth"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["jobDepth"].Visible = false;
            }

            if (checkBoxcountPcs.Checked)
            {
                dataGridDB.Columns["countPcs"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["countPcs"].Visible = false;
            }

            if (checkBoxlensType.Checked)
            {
                dataGridDB.Columns["lensType"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["lensType"].Visible = false;
            }

            if (checkBoxcountLayer.Checked)
            {
                dataGridDB.Columns["countLayer"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["countLayer"].Visible = false;
            }

            if (checkBoxsurfaceDimension.Checked)
            {
                dataGridDB.Columns["surfaceDimension"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["surfaceDimension"].Visible = false;
            }

            if (checkBoxhatch1type.Checked)
            {
                dataGridDB.Columns["hatch1type"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["hatch1type"].Visible = false;
            }

            if (checkBoxhatch2type.Checked)
            {
                dataGridDB.Columns["hatch2type"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["hatch2type"].Visible = false;
            }

            if (checkBoxtimeValuate.Checked)
            {
                dataGridDB.Columns["timeValuate"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["timeValuate"].Visible = false;
            }

            if (checkBoxtimeReal.Checked)
            {
                dataGridDB.Columns["timeReal"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["timeReal"].Visible = false;
            }

            if (checkBoxpositionZAxis.Checked)
            {
                dataGridDB.Columns["positionZAxis"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["positionZAxis"].Visible = false;
            }

            if (checkBoxcomment.Checked)
            {
                dataGridDB.Columns["comment"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["comment"].Visible = false;
            }

            if (checkBoxdimension.Checked)
            {
                dataGridDB.Columns["dimension"].Visible = true;
            }
            else
            {
                dataGridDB.Columns["dimension"].Visible = false;
            }
        }

        private void catchEnterInFindBox(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                searchUsingSomeTB();
            }
        }
    }
}
