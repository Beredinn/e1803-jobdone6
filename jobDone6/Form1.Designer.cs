﻿namespace jobDone6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label surfaceDimensionLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label dateStartLabel;
            System.Windows.Forms.Label dateStopLabel;
            System.Windows.Forms.Label jobTypeLabel;
            System.Windows.Forms.Label lensTypeLabel;
            System.Windows.Forms.Label commentLabel;
            System.Windows.Forms.Label orderCompanyLabel;
            System.Windows.Forms.Label orderNumberLabel;
            System.Windows.Forms.Label filePathLabel;
            System.Windows.Forms.Label pieceMaterialLabel;
            System.Windows.Forms.Label jobDepthLabel;
            System.Windows.Forms.Label countPcsLabel;
            System.Windows.Forms.Label countLayerLabel;
            System.Windows.Forms.Label hatch1TypeLabel;
            System.Windows.Forms.Label hatch2TypeLabel;
            System.Windows.Forms.Label timeOfJobLabel;
            System.Windows.Forms.Label timeOfJobRealLabel;
            System.Windows.Forms.Label positionZLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageShowImg = new System.Windows.Forms.TabPage();
            this.labelDimension = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonOpenFolderWithThisJob = new System.Windows.Forms.Button();
            this.tabPageEditAndAdd = new System.Windows.Forms.TabPage();
            this.DimensionTextBox = new System.Windows.Forms.TextBox();
            this.buttonCopyFromTableToNewJob = new System.Windows.Forms.Button();
            this.textBoxListOfFileInThisProject = new System.Windows.Forms.TextBox();
            this.buttonAddFileToThisJob = new System.Windows.Forms.Button();
            this.buttonFinishNewJob = new System.Windows.Forms.Button();
            this.surfaceDimensionTextBox = new System.Windows.Forms.TextBox();
            this.iDLabel1 = new System.Windows.Forms.Label();
            this.dateStartDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateStopDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.jobTypeComboBox = new System.Windows.Forms.ComboBox();
            this.lensTypeComboBox = new System.Windows.Forms.ComboBox();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.orderCompanyComboBox = new System.Windows.Forms.ComboBox();
            this.orderNumberTextBox = new System.Windows.Forms.TextBox();
            this.pieceMaterialComboBox = new System.Windows.Forms.ComboBox();
            this.jobDepthComboBox = new System.Windows.Forms.ComboBox();
            this.countPcsTextBox = new System.Windows.Forms.TextBox();
            this.countLayerTextBox = new System.Windows.Forms.TextBox();
            this.hatch1TypeTextBox = new System.Windows.Forms.TextBox();
            this.hatch2TypeTextBox = new System.Windows.Forms.TextBox();
            this.timeOfJobTextBox = new System.Windows.Forms.TextBox();
            this.timeOfJobRealTextBox = new System.Windows.Forms.TextBox();
            this.positionZTextBox = new System.Windows.Forms.TextBox();
            this.buttonNextStep = new System.Windows.Forms.Button();
            this.buttonAddNewJob = new System.Windows.Forms.Button();
            this.tabPageFindInDB = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxIDto = new System.Windows.Forms.TextBox();
            this.textBoxIDfrom = new System.Windows.Forms.TextBox();
            this.findJobTypeComboBox = new System.Windows.Forms.ComboBox();
            this.findLensTypeComboBox = new System.Windows.Forms.ComboBox();
            this.findCompanyComboBox = new System.Windows.Forms.ComboBox();
            this.findCompanyIDTextBox = new System.Windows.Forms.TextBox();
            this.findMaterialComboBox = new System.Windows.Forms.ComboBox();
            this.findJobDepthComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFindDateStop = new System.Windows.Forms.TextBox();
            this.buttonFindDateStop = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxFindDateStart = new System.Windows.Forms.TextBox();
            this.buttonFindDateStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSearchCompany = new System.Windows.Forms.Button();
            this.tabPagePrint = new System.Windows.Forms.TabPage();
            this.labelPrintReady = new System.Windows.Forms.Label();
            this.buttonGenrePrintableFileFromGrid = new System.Windows.Forms.Button();
            this.buttonShowPrintDialog = new System.Windows.Forms.Button();
            this.buttonPrintPreview = new System.Windows.Forms.Button();
            this.buttonSetupPage = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.tabPageShowInDBGrid = new System.Windows.Forms.TabPage();
            this.checkBoxlensType = new System.Windows.Forms.CheckBox();
            this.checkBoxcountPcs = new System.Windows.Forms.CheckBox();
            this.checkBoxjobDepth = new System.Windows.Forms.CheckBox();
            this.checkBoxjobType = new System.Windows.Forms.CheckBox();
            this.checkBoxmaterial = new System.Windows.Forms.CheckBox();
            this.checkBoxorderID = new System.Windows.Forms.CheckBox();
            this.checkBoxorderCompany = new System.Windows.Forms.CheckBox();
            this.checkBoxdateStop = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBoxdateStart = new System.Windows.Forms.CheckBox();
            this.dataGridDB = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateStop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.material = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobDepth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countPcs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lensType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countLayer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surfaceDimension = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hatch1type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hatch2type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeValuate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeReal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionZAxis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dimension = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.help1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonEditOneJob = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonChangeLogin = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.axScSamlightClientCtrl1 = new AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.checkBoxdimension = new System.Windows.Forms.CheckBox();
            this.checkBoxcomment = new System.Windows.Forms.CheckBox();
            this.checkBoxpositionZAxis = new System.Windows.Forms.CheckBox();
            this.checkBoxtimeReal = new System.Windows.Forms.CheckBox();
            this.checkBoxtimeValuate = new System.Windows.Forms.CheckBox();
            this.checkBoxhatch2type = new System.Windows.Forms.CheckBox();
            this.checkBoxhatch1type = new System.Windows.Forms.CheckBox();
            this.checkBoxsurfaceDimension = new System.Windows.Forms.CheckBox();
            this.checkBoxcountLayer = new System.Windows.Forms.CheckBox();
            surfaceDimensionLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            dateStartLabel = new System.Windows.Forms.Label();
            dateStopLabel = new System.Windows.Forms.Label();
            jobTypeLabel = new System.Windows.Forms.Label();
            lensTypeLabel = new System.Windows.Forms.Label();
            commentLabel = new System.Windows.Forms.Label();
            orderCompanyLabel = new System.Windows.Forms.Label();
            orderNumberLabel = new System.Windows.Forms.Label();
            filePathLabel = new System.Windows.Forms.Label();
            pieceMaterialLabel = new System.Windows.Forms.Label();
            jobDepthLabel = new System.Windows.Forms.Label();
            countPcsLabel = new System.Windows.Forms.Label();
            countLayerLabel = new System.Windows.Forms.Label();
            hatch1TypeLabel = new System.Windows.Forms.Label();
            hatch2TypeLabel = new System.Windows.Forms.Label();
            timeOfJobLabel = new System.Windows.Forms.Label();
            timeOfJobRealLabel = new System.Windows.Forms.Label();
            positionZLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPageShowImg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPageEditAndAdd.SuspendLayout();
            this.tabPageFindInDB.SuspendLayout();
            this.tabPagePrint.SuspendLayout();
            this.tabPageShowInDBGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDB)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axScSamlightClientCtrl1)).BeginInit();
            this.SuspendLayout();
            // 
            // surfaceDimensionLabel
            // 
            surfaceDimensionLabel.AutoSize = true;
            surfaceDimensionLabel.Location = new System.Drawing.Point(34, 321);
            surfaceDimensionLabel.Name = "surfaceDimensionLabel";
            surfaceDimensionLabel.Size = new System.Drawing.Size(73, 13);
            surfaceDimensionLabel.TabIndex = 57;
            surfaceDimensionLabel.Text = "Powierzchnia:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(44, 40);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(63, 13);
            iDLabel.TabIndex = 22;
            iDLabel.Text = "Nr zlecenia:";
            // 
            // dateStartLabel
            // 
            dateStartLabel.AutoSize = true;
            dateStartLabel.Location = new System.Drawing.Point(35, 66);
            dateStartLabel.Name = "dateStartLabel";
            dateStartLabel.Size = new System.Drawing.Size(72, 13);
            dateStartLabel.TabIndex = 24;
            dateStartLabel.Text = "Rozpoczęcie:";
            // 
            // dateStopLabel
            // 
            dateStopLabel.AutoSize = true;
            dateStopLabel.Location = new System.Drawing.Point(35, 90);
            dateStopLabel.Name = "dateStopLabel";
            dateStopLabel.Size = new System.Drawing.Size(72, 13);
            dateStopLabel.TabIndex = 26;
            dateStopLabel.Text = "Zakończenie:";
            // 
            // jobTypeLabel
            // 
            jobTypeLabel.AutoSize = true;
            jobTypeLabel.Location = new System.Drawing.Point(37, 193);
            jobTypeLabel.Name = "jobTypeLabel";
            jobTypeLabel.Size = new System.Drawing.Size(70, 13);
            jobTypeLabel.TabIndex = 28;
            jobTypeLabel.Text = "Typ zlecenia:";
            // 
            // lensTypeLabel
            // 
            lensTypeLabel.AutoSize = true;
            lensTypeLabel.Location = new System.Drawing.Point(47, 271);
            lensTypeLabel.Name = "lensTypeLabel";
            lensTypeLabel.Size = new System.Drawing.Size(60, 13);
            lensTypeLabel.TabIndex = 30;
            lensTypeLabel.Text = "Soczewka:";
            // 
            // commentLabel
            // 
            commentLabel.AutoSize = true;
            commentLabel.Location = new System.Drawing.Point(47, 496);
            commentLabel.Name = "commentLabel";
            commentLabel.Size = new System.Drawing.Size(60, 13);
            commentLabel.TabIndex = 32;
            commentLabel.Text = "Komentarz:";
            // 
            // orderCompanyLabel
            // 
            orderCompanyLabel.AutoSize = true;
            orderCompanyLabel.Location = new System.Drawing.Point(24, 116);
            orderCompanyLabel.Name = "orderCompanyLabel";
            orderCompanyLabel.Size = new System.Drawing.Size(83, 13);
            orderCompanyLabel.TabIndex = 34;
            orderCompanyLabel.Text = "Zleceniodawca:";
            // 
            // orderNumberLabel
            // 
            orderNumberLabel.AutoSize = true;
            orderNumberLabel.Location = new System.Drawing.Point(13, 141);
            orderNumberLabel.Name = "orderNumberLabel";
            orderNumberLabel.Size = new System.Drawing.Size(94, 13);
            orderNumberLabel.TabIndex = 36;
            orderNumberLabel.Text = "Nr zleceniodawcy:";
            // 
            // filePathLabel
            // 
            filePathLabel.AutoSize = true;
            filePathLabel.Location = new System.Drawing.Point(61, 521);
            filePathLabel.Name = "filePathLabel";
            filePathLabel.Size = new System.Drawing.Size(46, 13);
            filePathLabel.TabIndex = 38;
            filePathLabel.Text = "Katalog:";
            filePathLabel.Visible = false;
            // 
            // pieceMaterialLabel
            // 
            pieceMaterialLabel.AutoSize = true;
            pieceMaterialLabel.Location = new System.Drawing.Point(58, 167);
            pieceMaterialLabel.Name = "pieceMaterialLabel";
            pieceMaterialLabel.Size = new System.Drawing.Size(49, 13);
            pieceMaterialLabel.TabIndex = 40;
            pieceMaterialLabel.Text = "Materiał:";
            // 
            // jobDepthLabel
            // 
            jobDepthLabel.AutoSize = true;
            jobDepthLabel.Location = new System.Drawing.Point(44, 219);
            jobDepthLabel.Name = "jobDepthLabel";
            jobDepthLabel.Size = new System.Drawing.Size(63, 13);
            jobDepthLabel.TabIndex = 42;
            jobDepthLabel.Text = "Głębokość:";
            // 
            // countPcsLabel
            // 
            countPcsLabel.AutoSize = true;
            countPcsLabel.Location = new System.Drawing.Point(47, 245);
            countPcsLabel.Name = "countPcsLabel";
            countPcsLabel.Size = new System.Drawing.Size(60, 13);
            countPcsLabel.TabIndex = 44;
            countPcsLabel.Text = "Ilość sztuk:";
            // 
            // countLayerLabel
            // 
            countLayerLabel.AutoSize = true;
            countLayerLabel.Location = new System.Drawing.Point(39, 296);
            countLayerLabel.Name = "countLayerLabel";
            countLayerLabel.Size = new System.Drawing.Size(68, 13);
            countLayerLabel.TabIndex = 46;
            countLayerLabel.Text = "Ilość warstw:";
            // 
            // hatch1TypeLabel
            // 
            hatch1TypeLabel.AutoSize = true;
            hatch1TypeLabel.Location = new System.Drawing.Point(41, 346);
            hatch1TypeLabel.Name = "hatch1TypeLabel";
            hatch1TypeLabel.Size = new System.Drawing.Size(66, 13);
            hatch1TypeLabel.TabIndex = 48;
            hatch1TypeLabel.Text = "Hatch1 Typ:";
            // 
            // hatch2TypeLabel
            // 
            hatch2TypeLabel.AutoSize = true;
            hatch2TypeLabel.Location = new System.Drawing.Point(41, 371);
            hatch2TypeLabel.Name = "hatch2TypeLabel";
            hatch2TypeLabel.Size = new System.Drawing.Size(66, 13);
            hatch2TypeLabel.TabIndex = 50;
            hatch2TypeLabel.Text = "Hatch2 Typ:";
            // 
            // timeOfJobLabel
            // 
            timeOfJobLabel.AutoSize = true;
            timeOfJobLabel.Location = new System.Drawing.Point(18, 396);
            timeOfJobLabel.Name = "timeOfJobLabel";
            timeOfJobLabel.Size = new System.Drawing.Size(89, 13);
            timeOfJobLabel.TabIndex = 52;
            timeOfJobLabel.Text = "Czas szacowany:";
            // 
            // timeOfJobRealLabel
            // 
            timeOfJobRealLabel.AutoSize = true;
            timeOfJobRealLabel.Location = new System.Drawing.Point(18, 421);
            timeOfJobRealLabel.Name = "timeOfJobRealLabel";
            timeOfJobRealLabel.Size = new System.Drawing.Size(89, 13);
            timeOfJobRealLabel.TabIndex = 54;
            timeOfJobRealLabel.Text = "Czas rzeczywisty:";
            // 
            // positionZLabel
            // 
            positionZLabel.AutoSize = true;
            positionZLabel.Location = new System.Drawing.Point(37, 446);
            positionZLabel.Name = "positionZLabel";
            positionZLabel.Size = new System.Drawing.Size(70, 13);
            positionZLabel.TabIndex = 56;
            positionZLabel.Text = "Wysokość Z:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(59, 471);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(48, 13);
            label1.TabIndex = 125;
            label1.Text = "Rozmiar:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(25, 270);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(70, 13);
            label7.TabIndex = 113;
            label7.Text = "Typ zlecenia:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(35, 322);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(60, 13);
            label8.TabIndex = 114;
            label8.Text = "Soczewka:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(12, 193);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(83, 13);
            label9.TabIndex = 115;
            label9.Text = "Zleceniodawca:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(1, 218);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(94, 13);
            label10.TabIndex = 116;
            label10.Text = "Nr zleceniodawcy:";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(46, 244);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(49, 13);
            label11.TabIndex = 117;
            label11.Text = "Materiał:";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(32, 296);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(63, 13);
            label12.TabIndex = 118;
            label12.Text = "Głębokość:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(32, 168);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(63, 13);
            label13.TabIndex = 129;
            label13.Text = "Nr zlecenia:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageShowImg);
            this.tabControl1.Controls.Add(this.tabPageEditAndAdd);
            this.tabControl1.Controls.Add(this.tabPageFindInDB);
            this.tabControl1.Controls.Add(this.tabPagePrint);
            this.tabControl1.Controls.Add(this.tabPageShowInDBGrid);
            this.tabControl1.Location = new System.Drawing.Point(286, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(350, 413);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPageShowImg
            // 
            this.tabPageShowImg.Controls.Add(this.labelDimension);
            this.tabPageShowImg.Controls.Add(this.pictureBox1);
            this.tabPageShowImg.Controls.Add(this.buttonOpenFolderWithThisJob);
            this.tabPageShowImg.Location = new System.Drawing.Point(4, 22);
            this.tabPageShowImg.Name = "tabPageShowImg";
            this.tabPageShowImg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageShowImg.Size = new System.Drawing.Size(342, 387);
            this.tabPageShowImg.TabIndex = 1;
            this.tabPageShowImg.Text = "Podgląd";
            this.tabPageShowImg.UseVisualStyleBackColor = true;
            // 
            // labelDimension
            // 
            this.labelDimension.AutoSize = true;
            this.labelDimension.Location = new System.Drawing.Point(7, 37);
            this.labelDimension.Name = "labelDimension";
            this.labelDimension.Size = new System.Drawing.Size(0, 13);
            this.labelDimension.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(2, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(338, 344);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // buttonOpenFolderWithThisJob
            // 
            this.buttonOpenFolderWithThisJob.Location = new System.Drawing.Point(53, 6);
            this.buttonOpenFolderWithThisJob.Name = "buttonOpenFolderWithThisJob";
            this.buttonOpenFolderWithThisJob.Size = new System.Drawing.Size(250, 29);
            this.buttonOpenFolderWithThisJob.TabIndex = 2;
            this.buttonOpenFolderWithThisJob.Text = "Pobierz wszystkie dane do tego zlecenia";
            this.buttonOpenFolderWithThisJob.UseVisualStyleBackColor = true;
            this.buttonOpenFolderWithThisJob.Click += new System.EventHandler(this.buttonOpenFolderWithThisJob_Click);
            // 
            // tabPageEditAndAdd
            // 
            this.tabPageEditAndAdd.AutoScroll = true;
            this.tabPageEditAndAdd.Controls.Add(label1);
            this.tabPageEditAndAdd.Controls.Add(this.DimensionTextBox);
            this.tabPageEditAndAdd.Controls.Add(this.buttonCopyFromTableToNewJob);
            this.tabPageEditAndAdd.Controls.Add(this.textBoxListOfFileInThisProject);
            this.tabPageEditAndAdd.Controls.Add(this.buttonAddFileToThisJob);
            this.tabPageEditAndAdd.Controls.Add(this.buttonFinishNewJob);
            this.tabPageEditAndAdd.Controls.Add(surfaceDimensionLabel);
            this.tabPageEditAndAdd.Controls.Add(this.surfaceDimensionTextBox);
            this.tabPageEditAndAdd.Controls.Add(iDLabel);
            this.tabPageEditAndAdd.Controls.Add(this.iDLabel1);
            this.tabPageEditAndAdd.Controls.Add(dateStartLabel);
            this.tabPageEditAndAdd.Controls.Add(this.dateStartDateTimePicker);
            this.tabPageEditAndAdd.Controls.Add(dateStopLabel);
            this.tabPageEditAndAdd.Controls.Add(this.dateStopDateTimePicker);
            this.tabPageEditAndAdd.Controls.Add(jobTypeLabel);
            this.tabPageEditAndAdd.Controls.Add(this.jobTypeComboBox);
            this.tabPageEditAndAdd.Controls.Add(lensTypeLabel);
            this.tabPageEditAndAdd.Controls.Add(this.lensTypeComboBox);
            this.tabPageEditAndAdd.Controls.Add(commentLabel);
            this.tabPageEditAndAdd.Controls.Add(this.commentTextBox);
            this.tabPageEditAndAdd.Controls.Add(orderCompanyLabel);
            this.tabPageEditAndAdd.Controls.Add(this.orderCompanyComboBox);
            this.tabPageEditAndAdd.Controls.Add(orderNumberLabel);
            this.tabPageEditAndAdd.Controls.Add(this.orderNumberTextBox);
            this.tabPageEditAndAdd.Controls.Add(filePathLabel);
            this.tabPageEditAndAdd.Controls.Add(pieceMaterialLabel);
            this.tabPageEditAndAdd.Controls.Add(this.pieceMaterialComboBox);
            this.tabPageEditAndAdd.Controls.Add(jobDepthLabel);
            this.tabPageEditAndAdd.Controls.Add(this.jobDepthComboBox);
            this.tabPageEditAndAdd.Controls.Add(countPcsLabel);
            this.tabPageEditAndAdd.Controls.Add(this.countPcsTextBox);
            this.tabPageEditAndAdd.Controls.Add(countLayerLabel);
            this.tabPageEditAndAdd.Controls.Add(this.countLayerTextBox);
            this.tabPageEditAndAdd.Controls.Add(hatch1TypeLabel);
            this.tabPageEditAndAdd.Controls.Add(this.hatch1TypeTextBox);
            this.tabPageEditAndAdd.Controls.Add(hatch2TypeLabel);
            this.tabPageEditAndAdd.Controls.Add(this.hatch2TypeTextBox);
            this.tabPageEditAndAdd.Controls.Add(timeOfJobLabel);
            this.tabPageEditAndAdd.Controls.Add(this.timeOfJobTextBox);
            this.tabPageEditAndAdd.Controls.Add(timeOfJobRealLabel);
            this.tabPageEditAndAdd.Controls.Add(this.timeOfJobRealTextBox);
            this.tabPageEditAndAdd.Controls.Add(positionZLabel);
            this.tabPageEditAndAdd.Controls.Add(this.positionZTextBox);
            this.tabPageEditAndAdd.Controls.Add(this.buttonNextStep);
            this.tabPageEditAndAdd.Controls.Add(this.buttonAddNewJob);
            this.tabPageEditAndAdd.Location = new System.Drawing.Point(4, 22);
            this.tabPageEditAndAdd.Name = "tabPageEditAndAdd";
            this.tabPageEditAndAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEditAndAdd.Size = new System.Drawing.Size(342, 387);
            this.tabPageEditAndAdd.TabIndex = 0;
            this.tabPageEditAndAdd.Text = "Dodaj";
            this.tabPageEditAndAdd.UseVisualStyleBackColor = true;
            this.tabPageEditAndAdd.Click += new System.EventHandler(this.tabPageEditAndAdd_Click);
            // 
            // DimensionTextBox
            // 
            this.DimensionTextBox.Location = new System.Drawing.Point(107, 468);
            this.DimensionTextBox.Name = "DimensionTextBox";
            this.DimensionTextBox.Size = new System.Drawing.Size(200, 20);
            this.DimensionTextBox.TabIndex = 124;
            // 
            // buttonCopyFromTableToNewJob
            // 
            this.buttonCopyFromTableToNewJob.Location = new System.Drawing.Point(6, 157);
            this.buttonCopyFromTableToNewJob.Name = "buttonCopyFromTableToNewJob";
            this.buttonCopyFromTableToNewJob.Size = new System.Drawing.Size(35, 33);
            this.buttonCopyFromTableToNewJob.TabIndex = 123;
            this.buttonCopyFromTableToNewJob.Text = ">>>";
            this.buttonCopyFromTableToNewJob.UseVisualStyleBackColor = true;
            this.buttonCopyFromTableToNewJob.Click += new System.EventHandler(this.buttonReadDataFromTableAndWriteThisToNewJob_Click);
            // 
            // textBoxListOfFileInThisProject
            // 
            this.textBoxListOfFileInThisProject.Enabled = false;
            this.textBoxListOfFileInThisProject.Location = new System.Drawing.Point(110, 539);
            this.textBoxListOfFileInThisProject.Multiline = true;
            this.textBoxListOfFileInThisProject.Name = "textBoxListOfFileInThisProject";
            this.textBoxListOfFileInThisProject.Size = new System.Drawing.Size(200, 84);
            this.textBoxListOfFileInThisProject.TabIndex = 122;
            // 
            // buttonAddFileToThisJob
            // 
            this.buttonAddFileToThisJob.Location = new System.Drawing.Point(37, 537);
            this.buttonAddFileToThisJob.Name = "buttonAddFileToThisJob";
            this.buttonAddFileToThisJob.Size = new System.Drawing.Size(67, 86);
            this.buttonAddFileToThisJob.TabIndex = 121;
            this.buttonAddFileToThisJob.Text = "Dodaj plik (grafiki, pliki stl, dxf, stp itp)";
            this.buttonAddFileToThisJob.UseVisualStyleBackColor = true;
            this.buttonAddFileToThisJob.Click += new System.EventHandler(this.buttonAddFileToThisJob_Click);
            // 
            // buttonFinishNewJob
            // 
            this.buttonFinishNewJob.Enabled = false;
            this.buttonFinishNewJob.Location = new System.Drawing.Point(133, 10);
            this.buttonFinishNewJob.Name = "buttonFinishNewJob";
            this.buttonFinishNewJob.Size = new System.Drawing.Size(70, 23);
            this.buttonFinishNewJob.TabIndex = 102;
            this.buttonFinishNewJob.Text = "Zakończ";
            this.buttonFinishNewJob.UseVisualStyleBackColor = true;
            this.buttonFinishNewJob.Click += new System.EventHandler(this.button1_Click);
            // 
            // surfaceDimensionTextBox
            // 
            this.surfaceDimensionTextBox.Location = new System.Drawing.Point(107, 318);
            this.surfaceDimensionTextBox.Name = "surfaceDimensionTextBox";
            this.surfaceDimensionTextBox.Size = new System.Drawing.Size(200, 20);
            this.surfaceDimensionTextBox.TabIndex = 114;
            // 
            // iDLabel1
            // 
            this.iDLabel1.Location = new System.Drawing.Point(107, 40);
            this.iDLabel1.Name = "iDLabel1";
            this.iDLabel1.Size = new System.Drawing.Size(200, 19);
            this.iDLabel1.TabIndex = 23;
            // 
            // dateStartDateTimePicker
            // 
            this.dateStartDateTimePicker.CustomFormat = "d MMMM yyyy, HH:mm";
            this.dateStartDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateStartDateTimePicker.Location = new System.Drawing.Point(107, 63);
            this.dateStartDateTimePicker.Name = "dateStartDateTimePicker";
            this.dateStartDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateStartDateTimePicker.TabIndex = 104;
            // 
            // dateStopDateTimePicker
            // 
            this.dateStopDateTimePicker.CustomFormat = "d MMMM yyyy, HH:mm";
            this.dateStopDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateStopDateTimePicker.Location = new System.Drawing.Point(107, 88);
            this.dateStopDateTimePicker.Name = "dateStopDateTimePicker";
            this.dateStopDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateStopDateTimePicker.TabIndex = 105;
            // 
            // jobTypeComboBox
            // 
            this.jobTypeComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.jobTypeComboBox.DisplayMember = "jobTypeName";
            this.jobTypeComboBox.FormattingEnabled = true;
            this.jobTypeComboBox.Location = new System.Drawing.Point(107, 190);
            this.jobTypeComboBox.Name = "jobTypeComboBox";
            this.jobTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.jobTypeComboBox.TabIndex = 109;
            this.jobTypeComboBox.ValueMember = "jobTypeName";
            // 
            // lensTypeComboBox
            // 
            this.lensTypeComboBox.DisplayMember = "lensTypeName";
            this.lensTypeComboBox.FormattingEnabled = true;
            this.lensTypeComboBox.Location = new System.Drawing.Point(107, 267);
            this.lensTypeComboBox.Name = "lensTypeComboBox";
            this.lensTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.lensTypeComboBox.TabIndex = 112;
            this.lensTypeComboBox.ValueMember = "lensTypeName";
            // 
            // commentTextBox
            // 
            this.commentTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.commentTextBox.Location = new System.Drawing.Point(107, 493);
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(200, 20);
            this.commentTextBox.TabIndex = 120;
            // 
            // orderCompanyComboBox
            // 
            this.orderCompanyComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.orderCompanyComboBox.FormattingEnabled = true;
            this.orderCompanyComboBox.Location = new System.Drawing.Point(107, 113);
            this.orderCompanyComboBox.Name = "orderCompanyComboBox";
            this.orderCompanyComboBox.Size = new System.Drawing.Size(200, 21);
            this.orderCompanyComboBox.TabIndex = 106;
            // 
            // orderNumberTextBox
            // 
            this.orderNumberTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.orderNumberTextBox.Location = new System.Drawing.Point(107, 139);
            this.orderNumberTextBox.Name = "orderNumberTextBox";
            this.orderNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.orderNumberTextBox.TabIndex = 107;
            // 
            // pieceMaterialComboBox
            // 
            this.pieceMaterialComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.pieceMaterialComboBox.DisplayMember = "materialName";
            this.pieceMaterialComboBox.FormattingEnabled = true;
            this.pieceMaterialComboBox.Location = new System.Drawing.Point(107, 164);
            this.pieceMaterialComboBox.Name = "pieceMaterialComboBox";
            this.pieceMaterialComboBox.Size = new System.Drawing.Size(200, 21);
            this.pieceMaterialComboBox.TabIndex = 108;
            this.pieceMaterialComboBox.ValueMember = "materialName";
            // 
            // jobDepthComboBox
            // 
            this.jobDepthComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.jobDepthComboBox.DisplayMember = "jobDepthName";
            this.jobDepthComboBox.FormattingEnabled = true;
            this.jobDepthComboBox.Location = new System.Drawing.Point(107, 216);
            this.jobDepthComboBox.Name = "jobDepthComboBox";
            this.jobDepthComboBox.Size = new System.Drawing.Size(200, 21);
            this.jobDepthComboBox.TabIndex = 110;
            this.jobDepthComboBox.ValueMember = "jobDepthName";
            // 
            // countPcsTextBox
            // 
            this.countPcsTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.countPcsTextBox.Location = new System.Drawing.Point(107, 242);
            this.countPcsTextBox.Name = "countPcsTextBox";
            this.countPcsTextBox.Size = new System.Drawing.Size(200, 20);
            this.countPcsTextBox.TabIndex = 111;
            // 
            // countLayerTextBox
            // 
            this.countLayerTextBox.Location = new System.Drawing.Point(107, 293);
            this.countLayerTextBox.Name = "countLayerTextBox";
            this.countLayerTextBox.Size = new System.Drawing.Size(200, 20);
            this.countLayerTextBox.TabIndex = 113;
            // 
            // hatch1TypeTextBox
            // 
            this.hatch1TypeTextBox.Location = new System.Drawing.Point(107, 343);
            this.hatch1TypeTextBox.Name = "hatch1TypeTextBox";
            this.hatch1TypeTextBox.Size = new System.Drawing.Size(200, 20);
            this.hatch1TypeTextBox.TabIndex = 115;
            // 
            // hatch2TypeTextBox
            // 
            this.hatch2TypeTextBox.Location = new System.Drawing.Point(107, 368);
            this.hatch2TypeTextBox.Name = "hatch2TypeTextBox";
            this.hatch2TypeTextBox.Size = new System.Drawing.Size(200, 20);
            this.hatch2TypeTextBox.TabIndex = 116;
            // 
            // timeOfJobTextBox
            // 
            this.timeOfJobTextBox.Location = new System.Drawing.Point(107, 393);
            this.timeOfJobTextBox.Name = "timeOfJobTextBox";
            this.timeOfJobTextBox.Size = new System.Drawing.Size(200, 20);
            this.timeOfJobTextBox.TabIndex = 117;
            // 
            // timeOfJobRealTextBox
            // 
            this.timeOfJobRealTextBox.Location = new System.Drawing.Point(107, 418);
            this.timeOfJobRealTextBox.Name = "timeOfJobRealTextBox";
            this.timeOfJobRealTextBox.Size = new System.Drawing.Size(200, 20);
            this.timeOfJobRealTextBox.TabIndex = 118;
            // 
            // positionZTextBox
            // 
            this.positionZTextBox.Location = new System.Drawing.Point(107, 443);
            this.positionZTextBox.Name = "positionZTextBox";
            this.positionZTextBox.Size = new System.Drawing.Size(200, 20);
            this.positionZTextBox.TabIndex = 119;
            // 
            // buttonNextStep
            // 
            this.buttonNextStep.Enabled = false;
            this.buttonNextStep.Location = new System.Drawing.Point(209, 10);
            this.buttonNextStep.Name = "buttonNextStep";
            this.buttonNextStep.Size = new System.Drawing.Size(70, 23);
            this.buttonNextStep.TabIndex = 103;
            this.buttonNextStep.Text = "Wyślij";
            this.buttonNextStep.UseVisualStyleBackColor = true;
            this.buttonNextStep.Click += new System.EventHandler(this.buttonNextStep_Click);
            // 
            // buttonAddNewJob
            // 
            this.buttonAddNewJob.Enabled = false;
            this.buttonAddNewJob.Location = new System.Drawing.Point(57, 10);
            this.buttonAddNewJob.Name = "buttonAddNewJob";
            this.buttonAddNewJob.Size = new System.Drawing.Size(70, 23);
            this.buttonAddNewJob.TabIndex = 101;
            this.buttonAddNewJob.Text = "Rozpocznij";
            this.buttonAddNewJob.UseVisualStyleBackColor = true;
            this.buttonAddNewJob.Click += new System.EventHandler(this.buttonAddNewJob_Click);
            // 
            // tabPageFindInDB
            // 
            this.tabPageFindInDB.Controls.Add(label13);
            this.tabPageFindInDB.Controls.Add(this.label14);
            this.tabPageFindInDB.Controls.Add(this.label15);
            this.tabPageFindInDB.Controls.Add(this.textBoxIDto);
            this.tabPageFindInDB.Controls.Add(this.textBoxIDfrom);
            this.tabPageFindInDB.Controls.Add(label7);
            this.tabPageFindInDB.Controls.Add(this.findJobTypeComboBox);
            this.tabPageFindInDB.Controls.Add(label8);
            this.tabPageFindInDB.Controls.Add(this.findLensTypeComboBox);
            this.tabPageFindInDB.Controls.Add(label9);
            this.tabPageFindInDB.Controls.Add(this.findCompanyComboBox);
            this.tabPageFindInDB.Controls.Add(label10);
            this.tabPageFindInDB.Controls.Add(this.findCompanyIDTextBox);
            this.tabPageFindInDB.Controls.Add(label11);
            this.tabPageFindInDB.Controls.Add(this.findMaterialComboBox);
            this.tabPageFindInDB.Controls.Add(label12);
            this.tabPageFindInDB.Controls.Add(this.findJobDepthComboBox);
            this.tabPageFindInDB.Controls.Add(this.label6);
            this.tabPageFindInDB.Controls.Add(this.label5);
            this.tabPageFindInDB.Controls.Add(this.label4);
            this.tabPageFindInDB.Controls.Add(this.textBoxFindDateStop);
            this.tabPageFindInDB.Controls.Add(this.buttonFindDateStop);
            this.tabPageFindInDB.Controls.Add(this.label3);
            this.tabPageFindInDB.Controls.Add(this.textBoxFindDateStart);
            this.tabPageFindInDB.Controls.Add(this.buttonFindDateStart);
            this.tabPageFindInDB.Controls.Add(this.label2);
            this.tabPageFindInDB.Controls.Add(this.buttonSearchCompany);
            this.tabPageFindInDB.Location = new System.Drawing.Point(4, 22);
            this.tabPageFindInDB.Name = "tabPageFindInDB";
            this.tabPageFindInDB.Size = new System.Drawing.Size(342, 387);
            this.tabPageFindInDB.TabIndex = 2;
            this.tabPageFindInDB.Text = "Szukaj";
            this.tabPageFindInDB.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(203, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 128;
            this.label14.Text = "do";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(105, 168);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 127;
            this.label15.Text = "od";
            // 
            // textBoxIDto
            // 
            this.textBoxIDto.Location = new System.Drawing.Point(228, 164);
            this.textBoxIDto.Name = "textBoxIDto";
            this.textBoxIDto.Size = new System.Drawing.Size(67, 20);
            this.textBoxIDto.TabIndex = 126;
            this.textBoxIDto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // textBoxIDfrom
            // 
            this.textBoxIDfrom.Location = new System.Drawing.Point(130, 165);
            this.textBoxIDfrom.Name = "textBoxIDfrom";
            this.textBoxIDfrom.Size = new System.Drawing.Size(67, 20);
            this.textBoxIDfrom.TabIndex = 125;
            this.textBoxIDfrom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // findJobTypeComboBox
            // 
            this.findJobTypeComboBox.BackColor = System.Drawing.Color.White;
            this.findJobTypeComboBox.DisplayMember = "jobTypeName";
            this.findJobTypeComboBox.FormattingEnabled = true;
            this.findJobTypeComboBox.Location = new System.Drawing.Point(95, 267);
            this.findJobTypeComboBox.Name = "findJobTypeComboBox";
            this.findJobTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.findJobTypeComboBox.TabIndex = 122;
            this.findJobTypeComboBox.ValueMember = "jobTypeName";
            this.findJobTypeComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // findLensTypeComboBox
            // 
            this.findLensTypeComboBox.DisplayMember = "lensTypeName";
            this.findLensTypeComboBox.FormattingEnabled = true;
            this.findLensTypeComboBox.Location = new System.Drawing.Point(95, 319);
            this.findLensTypeComboBox.Name = "findLensTypeComboBox";
            this.findLensTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.findLensTypeComboBox.TabIndex = 124;
            this.findLensTypeComboBox.ValueMember = "lensTypeName";
            this.findLensTypeComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // findCompanyComboBox
            // 
            this.findCompanyComboBox.BackColor = System.Drawing.Color.White;
            this.findCompanyComboBox.FormattingEnabled = true;
            this.findCompanyComboBox.Location = new System.Drawing.Point(95, 190);
            this.findCompanyComboBox.Name = "findCompanyComboBox";
            this.findCompanyComboBox.Size = new System.Drawing.Size(200, 21);
            this.findCompanyComboBox.TabIndex = 119;
            this.findCompanyComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // findCompanyIDTextBox
            // 
            this.findCompanyIDTextBox.BackColor = System.Drawing.Color.White;
            this.findCompanyIDTextBox.Location = new System.Drawing.Point(95, 216);
            this.findCompanyIDTextBox.Name = "findCompanyIDTextBox";
            this.findCompanyIDTextBox.Size = new System.Drawing.Size(200, 20);
            this.findCompanyIDTextBox.TabIndex = 120;
            this.findCompanyIDTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // findMaterialComboBox
            // 
            this.findMaterialComboBox.BackColor = System.Drawing.Color.White;
            this.findMaterialComboBox.DisplayMember = "materialName";
            this.findMaterialComboBox.FormattingEnabled = true;
            this.findMaterialComboBox.Location = new System.Drawing.Point(95, 241);
            this.findMaterialComboBox.Name = "findMaterialComboBox";
            this.findMaterialComboBox.Size = new System.Drawing.Size(200, 21);
            this.findMaterialComboBox.TabIndex = 121;
            this.findMaterialComboBox.ValueMember = "materialName";
            this.findMaterialComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // findJobDepthComboBox
            // 
            this.findJobDepthComboBox.BackColor = System.Drawing.Color.White;
            this.findJobDepthComboBox.DisplayMember = "jobDepthName";
            this.findJobDepthComboBox.FormattingEnabled = true;
            this.findJobDepthComboBox.Location = new System.Drawing.Point(95, 293);
            this.findJobDepthComboBox.Name = "findJobDepthComboBox";
            this.findJobDepthComboBox.Size = new System.Drawing.Size(200, 21);
            this.findJobDepthComboBox.TabIndex = 123;
            this.findJobDepthComboBox.ValueMember = "jobDepthName";
            this.findJobDepthComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.catchEnterInFindBox);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(156, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 39);
            this.label6.TabIndex = 10;
            this.label6.Text = "cały rok\r\ncały miesiąc\r\nkonkretny dzień";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 39);
            this.label5.TabIndex = 9;
            this.label5.Text = "2018\r\n2018-08\r\n2018-08-01";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Format:";
            // 
            // textBoxFindDateStop
            // 
            this.textBoxFindDateStop.Location = new System.Drawing.Point(78, 39);
            this.textBoxFindDateStop.Name = "textBoxFindDateStop";
            this.textBoxFindDateStop.Size = new System.Drawing.Size(178, 20);
            this.textBoxFindDateStop.TabIndex = 7;
            this.textBoxFindDateStop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxFindDateStopKeyDown);
            // 
            // buttonFindDateStop
            // 
            this.buttonFindDateStop.Location = new System.Drawing.Point(262, 39);
            this.buttonFindDateStop.Name = "buttonFindDateStop";
            this.buttonFindDateStop.Size = new System.Drawing.Size(60, 20);
            this.buttonFindDateStop.TabIndex = 6;
            this.buttonFindDateStop.Text = "Szukaj";
            this.buttonFindDateStop.UseVisualStyleBackColor = true;
            this.buttonFindDateStop.Click += new System.EventHandler(this.buttonFindDateStop_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Zakończenie";
            // 
            // textBoxFindDateStart
            // 
            this.textBoxFindDateStart.Location = new System.Drawing.Point(78, 13);
            this.textBoxFindDateStart.Name = "textBoxFindDateStart";
            this.textBoxFindDateStart.Size = new System.Drawing.Size(178, 20);
            this.textBoxFindDateStart.TabIndex = 4;
            this.textBoxFindDateStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxFindDateStartKeyDown);
            // 
            // buttonFindDateStart
            // 
            this.buttonFindDateStart.Location = new System.Drawing.Point(262, 13);
            this.buttonFindDateStart.Name = "buttonFindDateStart";
            this.buttonFindDateStart.Size = new System.Drawing.Size(60, 20);
            this.buttonFindDateStart.TabIndex = 3;
            this.buttonFindDateStart.Text = "Szukaj";
            this.buttonFindDateStart.UseVisualStyleBackColor = true;
            this.buttonFindDateStart.Click += new System.EventHandler(this.buttonFindDateStart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Rozpoczęcie";
            // 
            // buttonSearchCompany
            // 
            this.buttonSearchCompany.Location = new System.Drawing.Point(95, 346);
            this.buttonSearchCompany.Name = "buttonSearchCompany";
            this.buttonSearchCompany.Size = new System.Drawing.Size(200, 23);
            this.buttonSearchCompany.TabIndex = 0;
            this.buttonSearchCompany.Text = "Szukaj (suma powyższych)";
            this.buttonSearchCompany.UseVisualStyleBackColor = true;
            this.buttonSearchCompany.Click += new System.EventHandler(this.buttonSearchCompany_Click);
            // 
            // tabPagePrint
            // 
            this.tabPagePrint.Controls.Add(this.labelPrintReady);
            this.tabPagePrint.Controls.Add(this.buttonGenrePrintableFileFromGrid);
            this.tabPagePrint.Controls.Add(this.buttonShowPrintDialog);
            this.tabPagePrint.Controls.Add(this.buttonPrintPreview);
            this.tabPagePrint.Controls.Add(this.buttonSetupPage);
            this.tabPagePrint.Controls.Add(this.buttonPrint);
            this.tabPagePrint.Location = new System.Drawing.Point(4, 22);
            this.tabPagePrint.Name = "tabPagePrint";
            this.tabPagePrint.Size = new System.Drawing.Size(342, 387);
            this.tabPagePrint.TabIndex = 3;
            this.tabPagePrint.Text = "Drukuj";
            this.tabPagePrint.UseVisualStyleBackColor = true;
            // 
            // labelPrintReady
            // 
            this.labelPrintReady.AutoSize = true;
            this.labelPrintReady.Location = new System.Drawing.Point(38, 59);
            this.labelPrintReady.Name = "labelPrintReady";
            this.labelPrintReady.Size = new System.Drawing.Size(10, 13);
            this.labelPrintReady.TabIndex = 24;
            this.labelPrintReady.Text = ".";
            // 
            // buttonGenrePrintableFileFromGrid
            // 
            this.buttonGenrePrintableFileFromGrid.Location = new System.Drawing.Point(16, 18);
            this.buttonGenrePrintableFileFromGrid.Name = "buttonGenrePrintableFileFromGrid";
            this.buttonGenrePrintableFileFromGrid.Size = new System.Drawing.Size(272, 34);
            this.buttonGenrePrintableFileFromGrid.TabIndex = 23;
            this.buttonGenrePrintableFileFromGrid.Text = "Generuj z wybranych zleceń";
            this.buttonGenrePrintableFileFromGrid.UseVisualStyleBackColor = true;
            this.buttonGenrePrintableFileFromGrid.Click += new System.EventHandler(this.buttonGenrePrintableFileFromGrid_Click);
            // 
            // buttonShowPrintDialog
            // 
            this.buttonShowPrintDialog.Location = new System.Drawing.Point(168, 105);
            this.buttonShowPrintDialog.Name = "buttonShowPrintDialog";
            this.buttonShowPrintDialog.Size = new System.Drawing.Size(120, 23);
            this.buttonShowPrintDialog.TabIndex = 22;
            this.buttonShowPrintDialog.Text = "Drukowanie";
            this.buttonShowPrintDialog.UseVisualStyleBackColor = true;
            this.buttonShowPrintDialog.Click += new System.EventHandler(this.buttonShowPrintDialog_Click);
            // 
            // buttonPrintPreview
            // 
            this.buttonPrintPreview.Location = new System.Drawing.Point(16, 105);
            this.buttonPrintPreview.Name = "buttonPrintPreview";
            this.buttonPrintPreview.Size = new System.Drawing.Size(120, 23);
            this.buttonPrintPreview.TabIndex = 21;
            this.buttonPrintPreview.Text = "Podgląd strony";
            this.buttonPrintPreview.UseVisualStyleBackColor = true;
            this.buttonPrintPreview.Click += new System.EventHandler(this.buttonPrintPreview_Click);
            // 
            // buttonSetupPage
            // 
            this.buttonSetupPage.Location = new System.Drawing.Point(16, 134);
            this.buttonSetupPage.Name = "buttonSetupPage";
            this.buttonSetupPage.Size = new System.Drawing.Size(120, 23);
            this.buttonSetupPage.TabIndex = 20;
            this.buttonSetupPage.Text = "Ustawienia strony";
            this.buttonSetupPage.UseVisualStyleBackColor = true;
            this.buttonSetupPage.Click += new System.EventHandler(this.buttonSetupPage_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(168, 134);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(120, 23);
            this.buttonPrint.TabIndex = 19;
            this.buttonPrint.Text = "Drukuj szybko";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // tabPageShowInDBGrid
            // 
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxdimension);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxcomment);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxpositionZAxis);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxtimeReal);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxtimeValuate);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxhatch2type);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxhatch1type);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxsurfaceDimension);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxcountLayer);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxlensType);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxcountPcs);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxjobDepth);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxjobType);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxmaterial);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxorderID);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxorderCompany);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxdateStop);
            this.tabPageShowInDBGrid.Controls.Add(this.label16);
            this.tabPageShowInDBGrid.Controls.Add(this.checkBoxdateStart);
            this.tabPageShowInDBGrid.Location = new System.Drawing.Point(4, 22);
            this.tabPageShowInDBGrid.Name = "tabPageShowInDBGrid";
            this.tabPageShowInDBGrid.Size = new System.Drawing.Size(342, 387);
            this.tabPageShowInDBGrid.TabIndex = 4;
            this.tabPageShowInDBGrid.Text = "Pokaż";
            this.tabPageShowInDBGrid.UseVisualStyleBackColor = true;
            // 
            // checkBoxlensType
            // 
            this.checkBoxlensType.AutoSize = true;
            this.checkBoxlensType.Location = new System.Drawing.Point(161, 110);
            this.checkBoxlensType.Name = "checkBoxlensType";
            this.checkBoxlensType.Size = new System.Drawing.Size(76, 17);
            this.checkBoxlensType.TabIndex = 9;
            this.checkBoxlensType.Text = "Soczewka";
            this.checkBoxlensType.UseVisualStyleBackColor = true;
            this.checkBoxlensType.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxcountPcs
            // 
            this.checkBoxcountPcs.AutoSize = true;
            this.checkBoxcountPcs.Location = new System.Drawing.Point(23, 110);
            this.checkBoxcountPcs.Name = "checkBoxcountPcs";
            this.checkBoxcountPcs.Size = new System.Drawing.Size(76, 17);
            this.checkBoxcountPcs.TabIndex = 8;
            this.checkBoxcountPcs.Text = "Ilość sztuk";
            this.checkBoxcountPcs.UseVisualStyleBackColor = true;
            this.checkBoxcountPcs.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxjobDepth
            // 
            this.checkBoxjobDepth.AutoSize = true;
            this.checkBoxjobDepth.Location = new System.Drawing.Point(198, 87);
            this.checkBoxjobDepth.Name = "checkBoxjobDepth";
            this.checkBoxjobDepth.Size = new System.Drawing.Size(79, 17);
            this.checkBoxjobDepth.TabIndex = 7;
            this.checkBoxjobDepth.Text = "Głębokość";
            this.checkBoxjobDepth.UseVisualStyleBackColor = true;
            this.checkBoxjobDepth.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxjobType
            // 
            this.checkBoxjobType.AutoSize = true;
            this.checkBoxjobType.Location = new System.Drawing.Point(106, 87);
            this.checkBoxjobType.Name = "checkBoxjobType";
            this.checkBoxjobType.Size = new System.Drawing.Size(86, 17);
            this.checkBoxjobType.TabIndex = 6;
            this.checkBoxjobType.Text = "Typ zlecenia";
            this.checkBoxjobType.UseVisualStyleBackColor = true;
            this.checkBoxjobType.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxmaterial
            // 
            this.checkBoxmaterial.AutoSize = true;
            this.checkBoxmaterial.Location = new System.Drawing.Point(23, 87);
            this.checkBoxmaterial.Name = "checkBoxmaterial";
            this.checkBoxmaterial.Size = new System.Drawing.Size(65, 17);
            this.checkBoxmaterial.TabIndex = 5;
            this.checkBoxmaterial.Text = "Materiał";
            this.checkBoxmaterial.UseVisualStyleBackColor = true;
            this.checkBoxmaterial.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxorderID
            // 
            this.checkBoxorderID.AutoSize = true;
            this.checkBoxorderID.Location = new System.Drawing.Point(161, 64);
            this.checkBoxorderID.Name = "checkBoxorderID";
            this.checkBoxorderID.Size = new System.Drawing.Size(110, 17);
            this.checkBoxorderID.TabIndex = 4;
            this.checkBoxorderID.Text = "Nr zleceniodawcy";
            this.checkBoxorderID.UseVisualStyleBackColor = true;
            this.checkBoxorderID.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxorderCompany
            // 
            this.checkBoxorderCompany.AutoSize = true;
            this.checkBoxorderCompany.Location = new System.Drawing.Point(23, 64);
            this.checkBoxorderCompany.Name = "checkBoxorderCompany";
            this.checkBoxorderCompany.Size = new System.Drawing.Size(99, 17);
            this.checkBoxorderCompany.TabIndex = 3;
            this.checkBoxorderCompany.Text = "Zleceniodawca";
            this.checkBoxorderCompany.UseVisualStyleBackColor = true;
            this.checkBoxorderCompany.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxdateStop
            // 
            this.checkBoxdateStop.AutoSize = true;
            this.checkBoxdateStop.Location = new System.Drawing.Point(161, 41);
            this.checkBoxdateStop.Name = "checkBoxdateStop";
            this.checkBoxdateStop.Size = new System.Drawing.Size(88, 17);
            this.checkBoxdateStop.TabIndex = 2;
            this.checkBoxdateStop.Text = "Zakończenie";
            this.checkBoxdateStop.UseVisualStyleBackColor = true;
            this.checkBoxdateStop.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(30, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(182, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Pokaż w tabeli następujące kolumny:";
            // 
            // checkBoxdateStart
            // 
            this.checkBoxdateStart.AutoSize = true;
            this.checkBoxdateStart.Location = new System.Drawing.Point(23, 41);
            this.checkBoxdateStart.Name = "checkBoxdateStart";
            this.checkBoxdateStart.Size = new System.Drawing.Size(88, 17);
            this.checkBoxdateStart.TabIndex = 0;
            this.checkBoxdateStart.Text = "Rozpoczęcie";
            this.checkBoxdateStart.UseVisualStyleBackColor = true;
            this.checkBoxdateStart.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // dataGridDB
            // 
            this.dataGridDB.AllowUserToAddRows = false;
            this.dataGridDB.AllowUserToDeleteRows = false;
            this.dataGridDB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridDB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.dateStart,
            this.dateStop,
            this.orderCompany,
            this.orderID,
            this.material,
            this.jobType,
            this.jobDepth,
            this.countPcs,
            this.lensType,
            this.countLayer,
            this.surfaceDimension,
            this.hatch1type,
            this.hatch2type,
            this.timeValuate,
            this.timeReal,
            this.positionZAxis,
            this.comment,
            this.dimension,
            this.help1});
            this.dataGridDB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dataGridDB.Location = new System.Drawing.Point(0, 25);
            this.dataGridDB.MultiSelect = false;
            this.dataGridDB.Name = "dataGridDB";
            this.dataGridDB.ReadOnly = true;
            this.dataGridDB.RowHeadersWidth = 32;
            this.dataGridDB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridDB.Size = new System.Drawing.Size(286, 387);
            this.dataGridDB.TabIndex = 3;
            // 
            // ID
            // 
            this.ID.HeaderText = "Nr zlecenia";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // dateStart
            // 
            this.dateStart.HeaderText = "Rozpoczęcie";
            this.dateStart.Name = "dateStart";
            this.dateStart.ReadOnly = true;
            this.dateStart.Width = 150;
            // 
            // dateStop
            // 
            this.dateStop.HeaderText = "Zakończenie";
            this.dateStop.Name = "dateStop";
            this.dateStop.ReadOnly = true;
            this.dateStop.Width = 150;
            // 
            // orderCompany
            // 
            this.orderCompany.HeaderText = "Zleceniodawca";
            this.orderCompany.Name = "orderCompany";
            this.orderCompany.ReadOnly = true;
            // 
            // orderID
            // 
            this.orderID.HeaderText = "Nr zleceniodawcy";
            this.orderID.Name = "orderID";
            this.orderID.ReadOnly = true;
            // 
            // material
            // 
            this.material.HeaderText = "Materiał";
            this.material.Name = "material";
            this.material.ReadOnly = true;
            // 
            // jobType
            // 
            this.jobType.HeaderText = "Typ zlecenia";
            this.jobType.Name = "jobType";
            this.jobType.ReadOnly = true;
            // 
            // jobDepth
            // 
            this.jobDepth.HeaderText = "Głębokość";
            this.jobDepth.Name = "jobDepth";
            this.jobDepth.ReadOnly = true;
            // 
            // countPcs
            // 
            this.countPcs.HeaderText = "Ilość sztuk";
            this.countPcs.Name = "countPcs";
            this.countPcs.ReadOnly = true;
            // 
            // lensType
            // 
            this.lensType.HeaderText = "Soczewka";
            this.lensType.Name = "lensType";
            this.lensType.ReadOnly = true;
            // 
            // countLayer
            // 
            this.countLayer.HeaderText = "Ilość warstw";
            this.countLayer.Name = "countLayer";
            this.countLayer.ReadOnly = true;
            // 
            // surfaceDimension
            // 
            this.surfaceDimension.HeaderText = "Powierzchnia";
            this.surfaceDimension.Name = "surfaceDimension";
            this.surfaceDimension.ReadOnly = true;
            // 
            // hatch1type
            // 
            this.hatch1type.HeaderText = "Hatch 1";
            this.hatch1type.Name = "hatch1type";
            this.hatch1type.ReadOnly = true;
            // 
            // hatch2type
            // 
            this.hatch2type.HeaderText = "Hatch 2";
            this.hatch2type.Name = "hatch2type";
            this.hatch2type.ReadOnly = true;
            // 
            // timeValuate
            // 
            this.timeValuate.HeaderText = "Czas szacow";
            this.timeValuate.Name = "timeValuate";
            this.timeValuate.ReadOnly = true;
            // 
            // timeReal
            // 
            this.timeReal.HeaderText = "Czas real";
            this.timeReal.Name = "timeReal";
            this.timeReal.ReadOnly = true;
            // 
            // positionZAxis
            // 
            this.positionZAxis.HeaderText = "Wysokość Z";
            this.positionZAxis.Name = "positionZAxis";
            this.positionZAxis.ReadOnly = true;
            // 
            // comment
            // 
            this.comment.HeaderText = "Komentarz";
            this.comment.Name = "comment";
            this.comment.ReadOnly = true;
            this.comment.Width = 200;
            // 
            // dimension
            // 
            this.dimension.HeaderText = "Rozmiar";
            this.dimension.Name = "dimension";
            this.dimension.ReadOnly = true;
            this.dimension.Visible = false;
            // 
            // help1
            // 
            this.help1.HeaderText = "Zapas";
            this.help1.Name = "help1";
            this.help1.ReadOnly = true;
            this.help1.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonRefresh,
            this.toolStripSeparator1,
            this.toolStripSeparator5,
            this.toolStripButtonEditOneJob,
            this.toolStripSeparator2,
            this.toolStripSeparator3,
            this.toolStripButtonChangeLogin,
            this.toolStripSeparator4});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(111, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRefresh.Image = global::jobDone6.Properties.Resources.refresh;
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRefresh.Text = "Odśwież";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.jobDBRefresh);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonEditOneJob
            // 
            this.toolStripButtonEditOneJob.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonEditOneJob.Image = global::jobDone6.Properties.Resources.edit;
            this.toolStripButtonEditOneJob.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEditOneJob.Name = "toolStripButtonEditOneJob";
            this.toolStripButtonEditOneJob.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonEditOneJob.Text = "Edytuj wpis";
            this.toolStripButtonEditOneJob.Click += new System.EventHandler(this.toolStripButtonEditOneJob_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonChangeLogin
            // 
            this.toolStripButtonChangeLogin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonChangeLogin.Image = global::jobDone6.Properties.Resources.profile;
            this.toolStripButtonChangeLogin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonChangeLogin.Name = "toolStripButtonChangeLogin";
            this.toolStripButtonChangeLogin.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonChangeLogin.Text = "Zmień login";
            this.toolStripButtonChangeLogin.Click += new System.EventHandler(this.toolStripButtonChangeLogin_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // axScSamlightClientCtrl1
            // 
            this.axScSamlightClientCtrl1.Enabled = true;
            this.axScSamlightClientCtrl1.Location = new System.Drawing.Point(262, 0);
            this.axScSamlightClientCtrl1.Name = "axScSamlightClientCtrl1";
            this.axScSamlightClientCtrl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axScSamlightClientCtrl1.OcxState")));
            this.axScSamlightClientCtrl1.Size = new System.Drawing.Size(24, 25);
            this.axScSamlightClientCtrl1.TabIndex = 5;
            this.axScSamlightClientCtrl1.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // checkBoxdimension
            // 
            this.checkBoxdimension.AutoSize = true;
            this.checkBoxdimension.Location = new System.Drawing.Point(23, 248);
            this.checkBoxdimension.Name = "checkBoxdimension";
            this.checkBoxdimension.Size = new System.Drawing.Size(64, 17);
            this.checkBoxdimension.TabIndex = 18;
            this.checkBoxdimension.Text = "Rozmiar";
            this.checkBoxdimension.UseVisualStyleBackColor = true;
            this.checkBoxdimension.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxcomment
            // 
            this.checkBoxcomment.AutoSize = true;
            this.checkBoxcomment.Location = new System.Drawing.Point(23, 225);
            this.checkBoxcomment.Name = "checkBoxcomment";
            this.checkBoxcomment.Size = new System.Drawing.Size(76, 17);
            this.checkBoxcomment.TabIndex = 17;
            this.checkBoxcomment.Text = "Komentarz";
            this.checkBoxcomment.UseVisualStyleBackColor = true;
            this.checkBoxcomment.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxpositionZAxis
            // 
            this.checkBoxpositionZAxis.AutoSize = true;
            this.checkBoxpositionZAxis.Location = new System.Drawing.Point(23, 202);
            this.checkBoxpositionZAxis.Name = "checkBoxpositionZAxis";
            this.checkBoxpositionZAxis.Size = new System.Drawing.Size(86, 17);
            this.checkBoxpositionZAxis.TabIndex = 16;
            this.checkBoxpositionZAxis.Text = "Wysokość Z";
            this.checkBoxpositionZAxis.UseVisualStyleBackColor = true;
            this.checkBoxpositionZAxis.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxtimeReal
            // 
            this.checkBoxtimeReal.AutoSize = true;
            this.checkBoxtimeReal.Location = new System.Drawing.Point(161, 179);
            this.checkBoxtimeReal.Name = "checkBoxtimeReal";
            this.checkBoxtimeReal.Size = new System.Drawing.Size(105, 17);
            this.checkBoxtimeReal.TabIndex = 15;
            this.checkBoxtimeReal.Text = "Czas rzeczywisty";
            this.checkBoxtimeReal.UseVisualStyleBackColor = true;
            this.checkBoxtimeReal.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxtimeValuate
            // 
            this.checkBoxtimeValuate.AutoSize = true;
            this.checkBoxtimeValuate.Location = new System.Drawing.Point(23, 179);
            this.checkBoxtimeValuate.Name = "checkBoxtimeValuate";
            this.checkBoxtimeValuate.Size = new System.Drawing.Size(105, 17);
            this.checkBoxtimeValuate.TabIndex = 14;
            this.checkBoxtimeValuate.Text = "Czas szacowany";
            this.checkBoxtimeValuate.UseVisualStyleBackColor = true;
            this.checkBoxtimeValuate.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxhatch2type
            // 
            this.checkBoxhatch2type.AutoSize = true;
            this.checkBoxhatch2type.Location = new System.Drawing.Point(161, 156);
            this.checkBoxhatch2type.Name = "checkBoxhatch2type";
            this.checkBoxhatch2type.Size = new System.Drawing.Size(61, 17);
            this.checkBoxhatch2type.TabIndex = 13;
            this.checkBoxhatch2type.Text = "Hatch2";
            this.checkBoxhatch2type.UseVisualStyleBackColor = true;
            this.checkBoxhatch2type.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxhatch1type
            // 
            this.checkBoxhatch1type.AutoSize = true;
            this.checkBoxhatch1type.Location = new System.Drawing.Point(23, 156);
            this.checkBoxhatch1type.Name = "checkBoxhatch1type";
            this.checkBoxhatch1type.Size = new System.Drawing.Size(61, 17);
            this.checkBoxhatch1type.TabIndex = 12;
            this.checkBoxhatch1type.Text = "Hatch1";
            this.checkBoxhatch1type.UseVisualStyleBackColor = true;
            this.checkBoxhatch1type.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxsurfaceDimension
            // 
            this.checkBoxsurfaceDimension.AutoSize = true;
            this.checkBoxsurfaceDimension.Location = new System.Drawing.Point(161, 133);
            this.checkBoxsurfaceDimension.Name = "checkBoxsurfaceDimension";
            this.checkBoxsurfaceDimension.Size = new System.Drawing.Size(89, 17);
            this.checkBoxsurfaceDimension.TabIndex = 11;
            this.checkBoxsurfaceDimension.Text = "Powierzchnia";
            this.checkBoxsurfaceDimension.UseVisualStyleBackColor = true;
            this.checkBoxsurfaceDimension.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // checkBoxcountLayer
            // 
            this.checkBoxcountLayer.AutoSize = true;
            this.checkBoxcountLayer.Location = new System.Drawing.Point(23, 133);
            this.checkBoxcountLayer.Name = "checkBoxcountLayer";
            this.checkBoxcountLayer.Size = new System.Drawing.Size(84, 17);
            this.checkBoxcountLayer.TabIndex = 10;
            this.checkBoxcountLayer.Text = "Ilość warstw";
            this.checkBoxcountLayer.UseVisualStyleBackColor = true;
            this.checkBoxcountLayer.CheckedChanged += new System.EventHandler(this.showDBGridColumns);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 412);
            this.Controls.Add(this.axScSamlightClientCtrl1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridDB);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(650, 450);
            this.Name = "Form1";
            this.Text = "jobDone - zapisywanie gotowych zleceń do bazy danych FREDEN";
            this.tabControl1.ResumeLayout(false);
            this.tabPageShowImg.ResumeLayout(false);
            this.tabPageShowImg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPageEditAndAdd.ResumeLayout(false);
            this.tabPageEditAndAdd.PerformLayout();
            this.tabPageFindInDB.ResumeLayout(false);
            this.tabPageFindInDB.PerformLayout();
            this.tabPagePrint.ResumeLayout(false);
            this.tabPagePrint.PerformLayout();
            this.tabPageShowInDBGrid.ResumeLayout(false);
            this.tabPageShowInDBGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDB)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axScSamlightClientCtrl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageShowImg;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonOpenFolderWithThisJob;
        private System.Windows.Forms.TabPage tabPageEditAndAdd;
        private System.Windows.Forms.Button buttonFinishNewJob;
        private System.Windows.Forms.TextBox surfaceDimensionTextBox;
        private System.Windows.Forms.Label iDLabel1;
        private System.Windows.Forms.DateTimePicker dateStartDateTimePicker;
        private System.Windows.Forms.DateTimePicker dateStopDateTimePicker;
        private System.Windows.Forms.ComboBox jobTypeComboBox;
        private System.Windows.Forms.ComboBox lensTypeComboBox;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.ComboBox orderCompanyComboBox;
        private System.Windows.Forms.TextBox orderNumberTextBox;
        private System.Windows.Forms.ComboBox pieceMaterialComboBox;
        private System.Windows.Forms.ComboBox jobDepthComboBox;
        private System.Windows.Forms.TextBox countPcsTextBox;
        private System.Windows.Forms.TextBox countLayerTextBox;
        private System.Windows.Forms.TextBox hatch1TypeTextBox;
        private System.Windows.Forms.TextBox hatch2TypeTextBox;
        private System.Windows.Forms.TextBox timeOfJobTextBox;
        private System.Windows.Forms.TextBox timeOfJobRealTextBox;
        private System.Windows.Forms.TextBox positionZTextBox;
        private System.Windows.Forms.Button buttonNextStep;
        private System.Windows.Forms.Button buttonAddNewJob;
        private System.Windows.Forms.TabPage tabPageFindInDB;
        private System.Windows.Forms.Button buttonSearchCompany;
        private System.Windows.Forms.DataGridView dataGridDB;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.Button buttonAddFileToThisJob;
        private AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl axScSamlightClientCtrl1;
        private System.Windows.Forms.TextBox textBoxListOfFileInThisProject;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonCopyFromTableToNewJob;
        private System.Windows.Forms.Label labelDimension;
        private System.Windows.Forms.TextBox DimensionTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateStop;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn material;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobType;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobDepth;
        private System.Windows.Forms.DataGridViewTextBoxColumn countPcs;
        private System.Windows.Forms.DataGridViewTextBoxColumn lensType;
        private System.Windows.Forms.DataGridViewTextBoxColumn countLayer;
        private System.Windows.Forms.DataGridViewTextBoxColumn surfaceDimension;
        private System.Windows.Forms.DataGridViewTextBoxColumn hatch1type;
        private System.Windows.Forms.DataGridViewTextBoxColumn hatch2type;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeValuate;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeReal;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionZAxis;
        private System.Windows.Forms.DataGridViewTextBoxColumn comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn dimension;
        private System.Windows.Forms.DataGridViewTextBoxColumn help1;
        private System.Windows.Forms.ToolStripButton toolStripButtonEditOneJob;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxFindDateStop;
        private System.Windows.Forms.Button buttonFindDateStop;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFindDateStart;
        private System.Windows.Forms.Button buttonFindDateStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox findJobTypeComboBox;
        private System.Windows.Forms.ComboBox findLensTypeComboBox;
        private System.Windows.Forms.ComboBox findCompanyComboBox;
        private System.Windows.Forms.TextBox findCompanyIDTextBox;
        private System.Windows.Forms.ComboBox findMaterialComboBox;
        private System.Windows.Forms.ComboBox findJobDepthComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripButton toolStripButtonChangeLogin;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxIDto;
        private System.Windows.Forms.TextBox textBoxIDfrom;
        private System.Windows.Forms.TabPage tabPagePrint;
        private System.Windows.Forms.Button buttonShowPrintDialog;
        private System.Windows.Forms.Button buttonPrintPreview;
        private System.Windows.Forms.Button buttonSetupPage;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonGenrePrintableFileFromGrid;
        private System.Windows.Forms.Label labelPrintReady;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TabPage tabPageShowInDBGrid;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBoxdateStart;
        private System.Windows.Forms.CheckBox checkBoxcountPcs;
        private System.Windows.Forms.CheckBox checkBoxjobDepth;
        private System.Windows.Forms.CheckBox checkBoxjobType;
        private System.Windows.Forms.CheckBox checkBoxmaterial;
        private System.Windows.Forms.CheckBox checkBoxorderID;
        private System.Windows.Forms.CheckBox checkBoxorderCompany;
        private System.Windows.Forms.CheckBox checkBoxdateStop;
        private System.Windows.Forms.CheckBox checkBoxlensType;
        private System.Windows.Forms.CheckBox checkBoxdimension;
        private System.Windows.Forms.CheckBox checkBoxcomment;
        private System.Windows.Forms.CheckBox checkBoxpositionZAxis;
        private System.Windows.Forms.CheckBox checkBoxtimeReal;
        private System.Windows.Forms.CheckBox checkBoxtimeValuate;
        private System.Windows.Forms.CheckBox checkBoxhatch2type;
        private System.Windows.Forms.CheckBox checkBoxhatch1type;
        private System.Windows.Forms.CheckBox checkBoxsurfaceDimension;
        private System.Windows.Forms.CheckBox checkBoxcountLayer;
    }
}

